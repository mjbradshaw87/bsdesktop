# import json
from jinja2 import Environment, FileSystemLoader
from PyQt5.QtWidgets import QWidget, QMessageBox, QTableWidgetItem, QAbstractItemView, QHeaderView
from PyQt5.QtCore import pyqtSignal, Qt, pyqtSlot, QUrl
import datetime
from dialogs.bbl_dialog import BblDialog
from dialogs.cellar_dialog import CellarDialog
from dialogs.kettle_dialog import KettleDialog
from dialogs.lauter_dialog import LauterDialog
from dialogs.mash_dialog import MashDialog
from dialogs.wort_dialog import WortDialog
from forms.ui_brew_tracking_widget import Ui_BrewTrackingWidget
from dialogs.brew_dialog import BrewDialog
from dialogs.recipe_dialog import RecipeDialog
from dialogs.note_dialog import NotesDialog
from data import const
from data.data_engine import find_data_file, format_datetime


JINJA_ENVIRONMENT = Environment(
    loader=FileSystemLoader(find_data_file('html_forms')),
    extensions=['jinja2.ext.autoescape', 'jinja2.ext.loopcontrols', 'jinja2.ext.do'],
    autoescape=True,
)
JINJA_ENVIRONMENT.filters['format_datetime'] = format_datetime


class BrewTrackingWidget(QWidget):
    """
    This is the class for the brew tracking widget.  This class will control all of
    the actions associated with the brew tracking widget, and reports to the main_window.py class
    """

    # SIGNALS
    became_busy = pyqtSignal(['bool', 'QString'], name='busy')

    # noinspection PyUnresolvedReferences
    def __init__(self):
        super(BrewTrackingWidget, self).__init__()

        # setup ui
        self.ui = Ui_BrewTrackingWidget()
        self.ui.setupUi(self)

        # setting up the webviews
        self.base_url = QUrl().fromLocalFile(find_data_file('html_forms/'))
        self.setup_webviews([
            self.ui.webview_brew_processing,
            self.ui.webview_information,
            self.ui.webview_notes
        ])

        # setup data_engine
        self.data_engine = None

        # data
        self.items = None
        self.recipes = None
        self.brews = None

        # behaviors
        self.ui.button_start_brew.setEnabled(False)
        self.ui.tab_widget_info.setEnabled(False)

        # setup signals
        self.ui.button_new_recipe.clicked.connect(self.new_recipe_clicked)
        self.ui.button_remove_recipe.clicked.connect(self.remove_recipe_clicked)
        self.ui.button_view_edit.clicked.connect(self.edit_recipe_dialog)
        self.ui.table_recipes.itemSelectionChanged.connect(self.activate_brew_button)
        self.ui.button_start_brew.clicked.connect(self.start_brew)
        self.ui.table_recipes.cellDoubleClicked.connect(self.edit_recipe_dialog)
        self.ui.table_active_brews.cellDoubleClicked.connect(self.edit_brew_dialog)
        self.ui.table_active_brews.itemSelectionChanged.connect(self.refresh_brew_sheet)

        # setup the recipe table and the active brew table
        self.setup_recipe_table()
        self.setup_active_brews()

    def setup_webviews(self, webviews):
        """
        This method sets up all the webviews on the form.
        :param webviews: a list with webviews
        :return: void
        """
        for webview in webviews:
            # disable the built-in context menu
            webview.setContextMenuPolicy(Qt.CustomContextMenu)

            # hide scrollbars
            webview.page().mainFrame().setScrollBarPolicy(Qt.Vertical, Qt.ScrollBarAlwaysOff)
            webview.page().mainFrame().setScrollBarPolicy(Qt.Horizontal, Qt.ScrollBarAlwaysOff)

    def refresh_information_tab(self, brew_index=0):
        """
        This method generates html content for the information tab and refreshes the view.
        :return: void
        """
        print(brew_index)
        # generate a template for the webview tab
        template = JINJA_ENVIRONMENT.get_template('information.html')
        data = {}

        # get the current brew
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]
        data['brew'] = brew

        # set up pyobjects in html
        self.ui.webview_information.setHtml(template.render(data), self.base_url)
        self.ui.webview_information.page().mainFrame().addToJavaScriptWindowObject('btw', self)

    def refresh_brew_processing_tab(self, brew_index=0):
        """
        This method generates html content for the brew processing tab and refreshes the view.
        :return: void
        """
        # generate a template for the webview tab
        template = JINJA_ENVIRONMENT.get_template('brew_processing.html')

        data = {}

        # get the current brew and its data
        brew = self.brews[brew_index]
        if len(brew['mash_info']):
            data['mash_info'] = brew['mash_info']
        else:
            data['mash_info'] = None

        if len(brew['lauter_info']):
            data['lauter_info'] = brew['lauter_info']
        else:
            data['lauter_info'] = None

        if len(brew['kettle_info']):
            data['kettle_info'] = brew['kettle_info']
        else:
            data['kettle_info'] = None

        if len(brew['cellar_info']):
            data['cellar_info'] = brew['cellar_info']
        else:
            data['cellar_info'] = None

        if len(brew['wort_info']):
            data['wort_info'] = brew['wort_info']
        else:
            data['wort_info'] = None

        # set up pyobjects in html
        self.ui.webview_brew_processing.setHtml(template.render(data), self.base_url)
        self.ui.webview_brew_processing.page().mainFrame().addToJavaScriptWindowObject('btw', self)

    def refresh_notes_tab(self, brew_index=0):
        """
        This method generates html content for the notes tab and refreshes the view.
        :return: void
        """
        # generate a template for the webview tab
        template = JINJA_ENVIRONMENT.get_template('notes.html')
        data = {'notes': []}

        # get the current brew
        brew = self.brews[brew_index]
        data['notes'] = brew['notes']

        # set up pyobjects in html
        self.ui.webview_notes.setHtml(template.render(data), self.base_url)
        self.ui.webview_notes.page().mainFrame().addToJavaScriptWindowObject('btw', self)

    def refresh_brew_sheet(self):
        """

        :return:
        """
        # find the selected index
        brew_index = self.ui.table_active_brews.currentIndex().row()

        # reload the new brew sheet
        self.refresh_brew_processing_tab(brew_index)
        self.refresh_information_tab(brew_index)
        self.refresh_notes_tab(brew_index)

###################################################
#               Brew Tracking html methods        #
###################################################

    @pyqtSlot(str)
    def show_brew_dialog(self):
        """

        :return: void
        """
        # detect the recipe to edit
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]

        # make the recipe dialog
        dialog = BrewDialog(
            data_engine=self.data_engine,
            recipe=None,
            brew=brew,
            parent=self
        )

        # setup the connection
        # noinspection PyUnresolvedReferences
        dialog.brew_info_updated.connect(self.brew_info_updated)

        # show the dialog
        dialog.open()

    def brew_info_updated(self, brew_info):
        """

        :param brew_info:
        :return:
        """
        # find the brew
        brew_index = self.ui.table_active_brews.currentIndex().row()

        # assign new_recipe_clicked mash info
        self.brews[brew_index] = brew_info

        # reload the new brew sheet
        self.refresh_information_tab(brew_index)
        self.brews_received({'brews': self.brews}, select_first_row=False)

###################################################
#               Brew Tracking html methods        #
###################################################

    @pyqtSlot(str)
    def show_mash_dialog(self):
        """

        :return: void
        """
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]

        dialog = MashDialog(
            data_engine=self.data_engine,
            brew=brew,
            parent=self
        )

        # set up the connection
        dialog.mash_info_updated.connect(self.mash_info_updated)

        # open the dialog
        dialog.open()

    def mash_info_updated(self, mash_info):
        """

        :param mash_info:
        :return:
        """
        # find the brew
        brew_index = self.ui.table_active_brews.currentIndex().row()
        brew = self.brews[brew_index]

        # assign new mash info
        brew['mash_info'] = mash_info

        # reload the new brew sheet
        self.refresh_brew_processing_tab(brew_index)

    @pyqtSlot(str)
    def show_lauter_dialog(self):
        """

        :return: void
        """
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]

        dialog = LauterDialog(
            data_engine=self.data_engine,
            brew=brew,
            parent=self
        )

        # set up the connection
        dialog.lauter_info_updated.connect(self.lauter_info_updated)

        # open the dialog
        dialog.open()

    def lauter_info_updated(self, lauter_info):
        """

        :param lauter_info:
        :return: void
        """
        # find the brew
        brew_index = self.ui.table_active_brews.currentIndex().row()
        brew = self.brews[brew_index]

        # assign new lauter info
        brew['lauter_info'] = lauter_info

        # reload the new brew sheet
        self.refresh_brew_processing_tab(brew_index)

    @pyqtSlot(str)
    def show_kettle_dialog(self):
        """

        :return: void
        """
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]

        dialog = KettleDialog(
            data_engine=self.data_engine,
            brew=brew,
            parent=self
        )

        # set up the connection
        dialog.kettle_info_updated.connect(self.kettle_info_updated)

        # open the dialog
        dialog.open()

    def kettle_info_updated(self, kettle_info):
        """

        :param kettle_info:
        :return: void
        """
        # find the brew
        brew_index = self.ui.table_active_brews.currentIndex().row()
        brew = self.brews[brew_index]

        # assign new kettle info
        brew['kettle_info'] = kettle_info

        # reload the new brew sheet
        self.refresh_brew_processing_tab(brew_index)

    @pyqtSlot(str)
    def show_cellar_dialog(self):
        """

        :return: void
        """
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]

        dialog = CellarDialog(
            data_engine=self.data_engine,
            brew=brew,
            parent=self
        )

        # set up the connection
        dialog.cellar_info_updated.connect(self.cellar_info_updated)

        # open the dialog
        dialog.open()

    def cellar_info_updated(self, cellar_info):
        """

        :param cellar_info:
        :return: void
        """
        # find the brew
        brew_index = self.ui.table_active_brews.currentIndex().row()
        brew = self.brews[brew_index]

        # assign new cellar info
        brew['cellar_info'] = cellar_info

        # reload the new brew sheet
        self.refresh_brew_processing_tab(brew_index)

    @pyqtSlot(str)
    def show_wort_dialog(self):
        """

        :return: void
        """
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]

        dialog = WortDialog(
            data_engine=self.data_engine,
            brew=brew,
            parent=self
        )

        # set up the connection
        dialog.wort_info_updated.connect(self.wort_info_updated)

        # open the dialog
        dialog.open()

    def wort_info_updated(self, wort_info):
        """

        :param wort_info:
        :return: void
        """
        # find the brew
        brew_index = self.ui.table_active_brews.currentIndex().row()
        brew = self.brews[brew_index]

        # assign new wort info
        brew['wort_info'] = wort_info

        # reload the new brew sheet
        self.refresh_brew_processing_tab(brew_index)

    @pyqtSlot(str)
    def show_mash_bbl_edit_dialog(self):
        """
        This is the slot to show a mash bbl dialog.  This is called from the
        html script, mash BBL.  This will pull the information and get the data form mash
        bbl levels.
        :return: void
        """
        dialog = BblDialog(self, data_engine=self.data_engine)
        dialog.open()

    @pyqtSlot(str)
    def show_lauter_bbl_edit_dialog(self):
        """
        This is the slot to show a lauter bbl dialog.  This is called from the
        html script, lauter BBL.  This will pull the information and get the data form mash
        bbl levels.
        :return: void
        """
        dialog = BblDialog(self, data_engine=self.data_engine)
        dialog.open()

    @pyqtSlot(str)
    def show_kettle_bbl_edit_dialog(self):
        """
        This is the slot to show a kettle bbl dialog.  This is called from the
        html script, lauter BBL.  This will pull the information and get the data form mash
        bbl levels.
        :return: void
        """
        dialog = BblDialog(self, data_engine=self.data_engine)
        dialog.open()

    @pyqtSlot(str)
    def show_htl_bbl_edit_dialog(self):
        """
        This is the slot to show a htl bbl dialog.  This is called from the
        html script, lauter BBL.  This will pull the information and get the data form mash
        bbl levels.
        :return: void
        """
        dialog = BblDialog(self, data_engine=self.data_engine)
        dialog.open()

    @pyqtSlot(str)
    def show_note_dialog(self, note_index):
        """
        This is the slot to show a lauter bbl dialog.  This is called from the
        html script, lauter BBL.  This will pull the information and get the data form mash
        bbl levels.
        :return: void
        """
        # get the brew
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]

        # get the note
        if note_index == 'NEW':  # edit mode
            note = None
        else:
            note = brew['notes'][int(note_index)]

        dialog = NotesDialog(
            data_engine=self.data_engine,
            brew_id=brew['id'],
            note=note,
            parent=self
        )

        # setup connection
        dialog.note_updated.connect(self.note_updated)

        dialog.open()

    def note_updated(self, note, is_new):
        """

        :return: void
        """
        # find the brew
        brew_index = self.ui.table_active_brews.currentIndex().row()
        brew = self.brews[brew_index]

        if is_new:
            brew['notes'].insert(0, note)
            # reload the new brew sheet
            self.refresh_notes_tab(brew_index)
        else:
            note_id = note['id']

            # find index and replace the old note with the new one
            note_index = -1
            for i, current_note in enumerate(brew['notes']):
                if current_note['id'] == note_id:
                    brew['notes'][i] = note
                    note_index = i
                    break

            if note_index >= 0:
                js_string = 'updateNote({noteIndex}, "{title}", "{content}");'.format(
                    noteIndex=note_index,
                    title=note['title'],
                    content=note['content']
                )
                self.ui.webview_notes.page().mainFrame().evaluateJavaScript(js_string)

    def setup_recipe_table(self):
        """

        :return: void
        """
        # setup table
        self.ui.table_recipes.setColumnCount(2)
        self.ui.table_recipes.setColumnHidden(1, True)
        self.ui.table_recipes.setColumnWidth(0, 250)
        self.ui.table_recipes.setHorizontalHeaderLabels(['Name'])
        self.ui.table_recipes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ui.table_recipes.setSelectionMode(QAbstractItemView.SingleSelection)
        header_view = self.ui.table_recipes.horizontalHeader()
        header_view.setSectionResizeMode(QHeaderView.Stretch)
        header_view.setSectionResizeMode(0, QHeaderView.Interactive)

    def setup_active_brews(self):
        """

        :return: void
        """
        self.ui.table_active_brews.setColumnCount(5)
        # self.ui.table_active_brews.setColumnHidden(1, True)
        self.ui.table_active_brews.setColumnWidth(0, 250)
        self.ui.table_active_brews.setHorizontalHeaderLabels([
            'Recipe Name',
            'Brew Number',
            'Batch Number',
            'FV Number',
            'Start Date'])
        self.ui.table_active_brews.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ui.table_active_brews.setSelectionMode(QAbstractItemView.SingleSelection)
        header_view = self.ui.table_active_brews.horizontalHeader()
        header_view.setSectionResizeMode(QHeaderView.Stretch)
        header_view.setSectionResizeMode(0, QHeaderView.Interactive)

    def set_data_engine(self, data_engine):
        """
        This method sets up the database then calls for the list of recipes
        :param data_engine: Data Engine object to call the server.
        :return: Nothing
        """
        # make the data engine
        self.data_engine = data_engine

#########################################################
#                         slots                         #
#########################################################

    def load_recipes_list(self, items):
        """

        :param items:
        :return: void
        """
        self.items = items

        # load recipes list
        self.request_recipes_list()

    def new_recipe_clicked(self):
        """
        This is the slot for the button_new_recipe 'clicked' signal.  When this
        receives a signal, it will make and show the dialog.
        :return: void
        """
        # make the new recipe dialog
        dialog_new_recipe = RecipeDialog(self.data_engine, self.items, parent=self)

        # set up connection
        # noinspection PyUnresolvedReferences
        dialog_new_recipe.accepted.connect(self.request_recipes_list)

        # show the dialog
        dialog_new_recipe.open()

    def edit_recipe_dialog(self):
        """
        This is slot to make or edit a recipe
        :return: void
        """
        # detect the recipe to edit
        row = self.ui.table_recipes.currentIndex().row()

        # make the recipe dialog
        dialog_edit_recipe = RecipeDialog(
            self.data_engine,
            self.items,
            edit_recipe=self.recipes[row],
            parent=self
        )

        # setup the connection
        # noinspection PyUnresolvedReferences
        dialog_edit_recipe.accepted.connect(self.request_recipes_list)

        # show the dialog
        dialog_edit_recipe.open()

    def edit_brew_dialog(self):
        """
        This is slot to edit a brew
        :return: void
        """
        # detect the recipe to edit
        brew = self.brews[self.ui.table_active_brews.currentIndex().row()]

        # make the recipe dialog
        edit_brew_dialog = BrewDialog(
            data_engine=self.data_engine,
            recipe=None,
            brew=brew,
            parent=self
        )

        # setup the connection
        # noinspection PyUnresolvedReferences
        edit_brew_dialog.accepted.connect(self.request_recipes_list)

        # show the dialog
        edit_brew_dialog.open()

    def remove_recipe_clicked(self):
        """
        This is slot for the remove button being clicked.  It will look for the id of the recipe
        and call to the server to remove a recipe.
        :return: void
        """
        # broadcast that the form is busy
        self.became_busy.emit(True, 'Removing an item...')

        # item you want to delete
        selected_recipe = self.recipes[self.ui.table_recipes.currentItem().row()]

        # data_engine connections
        self.data_engine.response_received.connect(self.recipe_removed_received)
        self.data_engine.got_error.connect(self.error_received)

        # send a request to the server
        self.data_engine.send_request(
            url=const.REMOVE_RECIPE_URL,
            params={'id': selected_recipe['id']},
        )

        # disable the remove button
        self.ui.button_remove_recipe.setEnabled(False)

    def start_brew(self):
        """
        This is the slot for the button_add being clicked by the user.  This will add a brew to the
        table_active_brew.
        :return: void
        """
        # item you want to add
        selected_recipe = self.recipes[self.ui.table_recipes.currentItem().row()]

        # setup a dialog
        dialog_new_brew = BrewDialog(
            self.data_engine,
            recipe=selected_recipe,
            parent=self.ui.table_active_brews
        )

        # setup the connection
        # noinspection PyUnresolvedReferences
        dialog_new_brew.accepted.connect(self.request_brews_list)

        # show new recipe dialog
        dialog_new_brew.open()

    def activate_brew_button(self):
        """
        This activates the start brew button
        :return: void
        """
        self.ui.button_start_brew.setEnabled(True)

#########################################################
#               date engine slots                       #
#########################################################

    def error_received(self, response):
        """
        This is the slot if the data_engine encounters an error.  If it does,
        then it will send with the signal a reason for the problem and this will log that problem,
        then display it.
        :return: void
        """
        # close connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # broadcast that the form is no longer busy
        self.became_busy.emit(False, '')

        # enable the remove button
        self.ui.button_remove_recipe.setEnabled(True)

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(response))
        box.exec_()

    def recipes_list_received(self, response):
        """
        This method is the slot for the successful retrieval of an items form the server.
        The server sends back a dictionary, the connection is closed, and the list_items widget
        is updated with the items form the dictionary.
        :param response: Response from the server if not an error. A dictionary of items
        :return: void
        """
        # save a list of recipes
        self.recipes = response['recipes']
        recipes_names = map(lambda current_recipe: current_recipe['name'], self.recipes)

        # broadcast that the form is not busy
        self.became_busy.emit(False, '')

        # disconnect for the engine
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # clear the table
        self.ui.table_recipes.clear()

        # make the new table
        self.ui.table_recipes.setRowCount(len(self.recipes))

        # set items in the list
        for i, recipe in enumerate(recipes_names):
            node_name = QTableWidgetItem(recipe)
            # node_supplier = QTableWidgetItem(recipe.supplier)
            # node_price = QTableWidgetItem(str(recipe.price))
            self.ui.table_recipes.setItem(i, 0, node_name)
            # self.ui.table_recipes.setItem(i, 1, node_supplier)
            # self.ui.table_recipes.setItem(i, 2, node_price)
            self.ui.table_recipes.setItem(i, 3, QTableWidgetItem(str(i)))

        # request brews list
        self.request_brews_list()

    def recipe_removed_received(self):
        """
        This is the slot for the remove recipe button.  This slot will emit that the form
        is not busy, disconnect the signals, enables the buttona and refresh the list.
        :return: void
        """
        # broadcast that the form is not busy
        self.became_busy.emit(False, '')

        # disconnect for the engine
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # enable the remove button
        self.ui.button_remove_recipe.setEnabled(True)

        # request a new list
        self.request_recipes_list()

#########################################################
#                 internal methods                      #
#########################################################

    def request_recipes_list(self):
        """
        This method gets the users entire inventory and passes it to the app so
        their inventory can be displayed in the list_items widget.
        :return: void
        """
        # data_engine connections
        self.data_engine.response_received.connect(self.recipes_list_received)
        self.data_engine.got_error.connect(self.error_received)

        # broadcast that the form is busy
        self.became_busy.emit(True, 'Updating your recipes...')

        # send a request to the server for an items dictionary
        self.data_engine.send_request(
            url=const.GET_ALL_RECIPES_URL,
            params={'sort_by': 'name'},
        )

    def request_brews_list(self):
        """
        This method gets the users entire inventory and passes it to the app so
        their inventory can be displayed in the list_items widget.
        :return: void
        """
        # data_engine connections
        self.data_engine.response_received.connect(self.brews_received)
        self.data_engine.got_error.connect(self.error_received)

        # broadcast that the form is busy
        self.became_busy.emit(True, 'Updating your open brews list...')

        # send a request to the server for an items dictionary
        self.data_engine.send_request(
            url=const.ALL_BREWS_URL,
            params={'sort_by': 'date_created'},
        )

    def brews_received(self, response, select_first_row=True):
        """
        This method is the slot for the successful retrieval of an items form the server.
        The server sends back a dictionary, the connection is closed, and the list_items widget
        is updated with the items form the dictionary.
        :param response: Response from the server if not an error. A dictionary of items
        :return: void
        """
        # save a list of recipes
        self.brews = response['brews']

        # broadcast that the form is not busy
        self.became_busy.emit(False, '')

        # disconnect for the engine
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # clear the table
        # self.ui.table_active_brews.clear()

        # make the new table
        self.ui.table_active_brews.setRowCount(len(self.brews))

        # set items in the list
        recipes_ids = list(map(lambda recipe: recipe['id'], self.recipes))

        for i, brew in enumerate(self.brews):
            recipe_name = self.recipes[recipes_ids.index(brew['recipe_id'])]['name']
            node_name = QTableWidgetItem(recipe_name)
            node_brew_number = QTableWidgetItem(brew['brew_code'])
            node_batch_number = QTableWidgetItem(brew['batch_number'])
            node_fv_number = QTableWidgetItem(brew['fv_number'])
            date_value = datetime.datetime.fromtimestamp(float(brew['date_created']))
            node_date_started = QTableWidgetItem(date_value.strftime('%H:%M %p %m/%d/%y'))
            self.ui.table_active_brews.setItem(i, 0, node_name)
            self.ui.table_active_brews.setItem(i, 1, node_brew_number)
            self.ui.table_active_brews.setItem(i, 2, node_batch_number)
            self.ui.table_active_brews.setItem(i, 3, node_fv_number)
            self.ui.table_active_brews.setItem(i, 4, node_date_started)

        # set the selection and highlight
        if select_first_row:
            self.ui.tab_widget_info.setEnabled(True)
            self.ui.table_active_brews.selectRow(0)
        elif len(self.brews):
            self.ui.tab_widget_info.setEnabled(True)

            # refresh the content
            self.refresh_information_tab()
            self.refresh_brew_processing_tab()
            self.refresh_notes_tab()
