from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import (
    QMainWindow, QStatusBar, QLabel, QMessageBox, QWidget, QVBoxLayout, QTabWidget, QDesktopWidget
)
from auth.authentication_dialog import AuthenticationDialog
from custom_controls.qprogressindicator import QProgressIndicator
from data.data_engine import DataEngine
from .inventory_control_widget import InventoryControlWidget
from .brew_tracking_widget import BrewTrackingWidget
from data import const


class MainWindow(QMainWindow):
    """
    This is the main window class, this controls all of the activity for the app.
    """
    def __init__(self):
        """
        Constructor for the main window app. It will create all the necessary objects for the app to launch,
        create the main tabs, the data engine, load or save settings and will decide if the user
        will need to first enter the auth widget to get credentials.
        :return: void
        """
        super(MainWindow, self).__init__()

        # set the app name
        self.setWindowTitle(const.APP_NAME + ' ' + const.APP_VERSION)

        # set up the central widget
        self.central_widget = QTabWidget()

        # set main layout for the central widget
        main_layout = QVBoxLayout()
        main_layout.setContentsMargins(6, 12, 6, 3)
        main_layout.addSpacing(5)
        main_layout.addWidget(self.central_widget)
        main_widget = QWidget()
        main_widget.setLayout(main_layout)

        # set up the status bar
        self.status_bar = QStatusBar()
        self.progress = QProgressIndicator(self.status_bar)
        self.label_info = QLabel()
        self.status_bar.addWidget(self.progress)
        self.status_bar.addWidget(self.label_info)

        # set main window components
        self.setCentralWidget(main_widget)
        self.setStatusBar(self.status_bar)

        # resize the main window
        self.resize(700, 700)

        # set up variables
        self.user_info = None
        self.data_engine = None
        self.inventory_control_widget = InventoryControlWidget()
        self.brew_tracking_widget = BrewTrackingWidget()

        # setup the tabs
        self.central_widget.addTab(self.inventory_control_widget, 'Inventory Control')
        self.central_widget.addTab(self.brew_tracking_widget, 'Brew Tracking')

        # set up connections
        self.inventory_control_widget.became_busy.connect(self.inventory_control_busy)
        self.inventory_control_widget.got_items_list.connect(self.brew_tracking_widget.load_recipes_list)

    def start_app(self):
        """
        This method launches the app
        :return:
        """
        # load settings
        has_position_settings = self.load_settings()

        if not has_position_settings:
            # place in the middle of the screen
            frame = self.frameGeometry()
            desktop_center = QDesktopWidget().availableGeometry().center()
            frame.moveCenter(desktop_center)
            self.move(frame.topLeft())

        self.show()

        # run authorization, if needed
        if self.user_info:
            self.setup_engine()
        else:
            # request authorization
            self.show_auth_dialog()

    def closeEvent(self, event):
        """
        This is the event when the program exits.  It will prompt the user to make sure
        they really want to exit the program.  If the user says yes, the event will call an internal
        method to save the users settings.  If the user says no, the event will ignore the call.
        :param event: The action to close the form
        :return: void
        """
        self.save_settings()

        if 1:

            event.accept()
            return
        # exit message for the user
        exit_message = 'Are you sure you want to exit BeerScribe?'

        # set up the QMessageBox to prompt the user
        exit_box = QMessageBox(self)
        exit_box.setText(exit_message)

        # ask the user if they want to exit the program
        reply = exit_box.question(
            self,
            'Exit Confirmation',
            exit_message,
            QMessageBox.Yes,
            QMessageBox.No
        )

        # Action of the QMessageBox.  Depends on the user input.  If the user selects to exit
        # the app, the save settings method will be called.  If the user chooses to ignore
        # the event, it will do nothing.
        if reply == QMessageBox.Yes:
            self.save_settings()
            event.accept()
        else:
            event.ignore()

    def busy(self, is_busy, operation_string):
        """
        This is the method to display a progress indicator on the widget.  This method
        will be called if the program is awaiting a response from the server.
        :param is_busy: Bool value to indicate if the form is in the middle of making a call
        to the server.
        :return: void
        """
        # start/stop animation
        self.progress.startAnimation() if is_busy else self.progress.stopAnimation()

        # set the operation description
        self.label_info.setText(operation_string) if is_busy else self.label_info.clear()

    def inventory_control_busy(self, is_busy, operation_string):
        """
        This is the slot to control the progress display at the bottom of the app.  It will let the
        user know of any actions taken by the app.
        :param is_busy: bool, sets the form as busy or not.
        :param operation_string: string, gives the user a breif summary of the actions being taken
        by the app.
        :return: void
        """
        self.busy(is_busy, operation_string)

    def show_auth_dialog(self):
        """
        This method will show the Authentication Dialog if the user does not have the
        correct credentials
        :return: void
        """
        # show auth dialog, in case of success, we have credentials
        auth_dialog = AuthenticationDialog(self)
        if auth_dialog.exec_():
            self.user_info_received(auth_dialog.user_info)
        else:
            self.deleteLater()

    def setup_engine(self):
        """
        Creates the data engine for the app and the main widget.  When created, this method
        will also set up the slot for the response received signal for the data engine.
        :return: void
        """
        # create data_engine
        self.data_engine = DataEngine(refresh_token=self.user_info['tokens']['refresh_token'])
        self.data_engine.user_does_not_exist.connect(self.user_invalid_or_expired)

        # pass the data_engine into inventory control widget
        self.inventory_control_widget.set_data_engine(self.data_engine)
        self.brew_tracking_widget.set_data_engine(self.data_engine)

    def user_info_received(self, user_info):
        """
        This is a method to set up the users information and will call a method to setup the data_engine
        :param user_info: user credentials
        :return: void
        """
        # save credentials
        self.user_info = user_info

        # save settings with user info
        self.save_settings()

        # setup the data engine
        self.setup_engine()

    def remove_tokens(self):
        """
        This method will remove the tokens of the user is not found bt the server
        :return: void
        """
        # Find the settings info
        settings = QSettings('Scribe Software Solutions', 'BeerScribe')

        # if the settings contain user and tokens remove them
        if settings.contains('user') and settings.contains('tokens'):
            settings.remove('user')
            settings.remove('tokens')

    def user_invalid_or_expired(self):
        """
        This is the method called if the suer is invalid or they have a token
        that is not in the server.  If the user is not found, or the tokens are not found,  Then
        the data engine will disconnect and the tokens will be removed by another method called
        'remove tokens'
        :return: Nothing
        """
        # destroy the existing data engine
        self.user_info = None
        self.data_engine.user_does_not_exist.disconnect()
        self.data_engine = None

        # clear tokens from settings
        self.remove_tokens()

        # make the messagebox and set the text
        box = QMessageBox(self)
        box.setText('Your credentials are not valid.\n'
                    'You are being sent to the authentication process, please sign in again.')

        box.exec_()

        # run auth widget again
        self.show_auth_dialog()

    def save_settings(self):
        """
        This method saves the users settings to load the form in its current position,
        open tabs and tokens.
        :return: void
        """
        settings = QSettings('Scribe Software Solutions', 'BeerScribe')
        settings.setValue('geometry', self.saveGeometry())
        settings.setValue('state', self.saveState())
        settings.setValue('position', self.pos())
        settings.setValue('current_tab', self.central_widget.currentIndex())
        settings.setValue('current_tab_bottom', self.brew_tracking_widget.ui.tab_widget_info.currentIndex())
        settings.setValue('user', self.user_info['user'])
        settings.setValue('tokens', self.user_info['tokens'])

        del settings

    def load_settings(self):
        """
        Method for loading the users settings from the last event.  If it is a new user, the
        settings will not be loaded.
        :return: void
        """
        has_position = True

        settings = QSettings('Scribe Software Solutions', 'BeerScribe')
        # settings.clear()

        # window position and state
        if settings.contains('geometry'):
            geometry = settings.value('geometry')
            self.restoreGeometry(geometry.data())

        if settings.contains('state'):
            state = settings.value('state')
            self.restoreState(state.data())

        if settings.contains('position'):
            position = settings.value('position')
            self.move(position)
        else:
            has_position = False

        # tab choice
        if settings.contains('current_tab'):
            self.central_widget.setCurrentIndex(int(settings.value('current_tab')))

        # bottom tab choice
        if settings.contains('current_tab_bottom'):
            self.brew_tracking_widget.ui.tab_widget_info.setCurrentIndex(int(settings.value('current_tab_bottom')))

        # user tokens and info
        if settings.contains('user') and settings.contains('tokens'):
            self.user_info = {}
            user = settings.value('user')
            user = dict((str(k), v) for k, v in user.items())
            tokens = settings.value('tokens')
            tokens = dict((str(k), v) for k, v in tokens.items())

            self.user_info['user'] = user
            self.user_info['tokens'] = tokens

        return has_position
