from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget, QAbstractItemView, QHeaderView, QMessageBox, QTableWidgetItem
from forms.ui_inventory_control_widget import Ui_InventoryControlWidget
from dialogs.inventory_item_dialog import InventoryItemDialog
from data import const
from data.item import Item


class InventoryControlWidget(QWidget):
    """
    This is the inventoryControlWidget, it is the widget for the Inventory Control
    tab on the main_window Widget.  It holds all the data for a users inventory
    and allows them to control their stock.
    """
    # signals
    became_busy = pyqtSignal(['bool', 'QString'], name='busy')
    got_items_list = pyqtSignal(list)

    # noinspection PyUnresolvedReferences
    def __init__(self):
        """
        This is the constructor for the InventoryControlWidget
        :return: void
        """
        super(InventoryControlWidget, self).__init__()

        # set up UI
        self.ui = Ui_InventoryControlWidget()
        self.ui.setupUi(self)

        # setup objects
        self.data_engine = None
        self.items = None
        self.units = None
        self.classes = None

        # setup widget behaviors
        self.ui.button_remove.setEnabled(False)
        self.ui.button_edit.setEnabled(False)

        # setup table
        self.ui.table_items.setColumnCount(7)
        self.ui.table_items.setColumnHidden(6, True)
        self.ui.table_items.setColumnWidth(0, 200)
        self.ui.table_items.setColumnWidth(1, 200)
        self.ui.table_items.setColumnWidth(2, 150)
        self.ui.table_items.setColumnWidth(3, 150)
        self.ui.table_items.setColumnWidth(4, 150)
        self.ui.table_items.setColumnWidth(5, 150)
        self.ui.table_items.setHorizontalHeaderLabels(
            ['Name', 'Class', 'Supplier', 'Amount', 'Price', 'Addtional Info']
        )
        self.ui.table_items.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ui.table_items.setSelectionMode(QAbstractItemView.SingleSelection)
        self.ui.table_items.horizontalHeader().setSectionResizeMode(5, QHeaderView.Stretch)
        # header_view.setResizeMode(QHeaderView.Stretch)
        # header_view.setResizeMode(0, QHeaderView.Interactive)
        self.edit_item = None

        # setup inventory control connections
        self.ui.button_add.clicked.connect(self.show_item_dialog)
        self.ui.button_remove.clicked.connect(self.remove_item)
        self.ui.button_edit.clicked.connect(self.edit_item_dialog)
        # setup widget_search connections
        self.ui.line_search.textChanged.connect(self.line_search_text_changed)

        # setup widget_data_connections
        self.ui.table_items.itemSelectionChanged.connect(self.remove_button_state)
        self.ui.table_items.cellDoubleClicked.connect(self.edit_item_dialog)
        self.became_busy.connect(self.enable_form)

    def set_data_engine(self, data_engine):
        """
        This method sets up the date engine to call the server.
        :param data_engine: Data Engine object to call the server.
        :return: void
        """
        # make the data engine
        self.data_engine = data_engine

        # refresh items in the list
        self.request_items_list()

#########################################################
#                         slots                         #
#########################################################

    def show_item_dialog(self):
        """
        This method will show the inventory item dialog to guide the user in setting up an item.  If
        the user accepts the item, then it will send a signal to the request items list method
        :return: void
        """
        # setup dialog
        add_item_dialog = InventoryItemDialog(self.data_engine, units=self.units, classes=self.classes, parent=self)

        # noinspection PyUnresolvedReferences
        add_item_dialog.accepted.connect(self.request_items_list)

        # show new recipe dialog
        add_item_dialog.open()

    def remove_item(self):
        """
        This method sets up the signals for button_remove.  It will pop up a dialog to make sure the user
        does want to remove the item, and if yes is selected will call the server and remove the item.
        :return: void
        """
        # exit message for the user
        remove_message = 'Are you sure you want to remove this item?'

        # set up the QMessageBox to prompt the user
        remove_box = QMessageBox(self)
        remove_box.setText(remove_message)

        # ask the user if they want to exit the program
        reply = remove_box.question(
            self,
            'Remove Confirmation',
            remove_message,
            QMessageBox.Yes,
            QMessageBox.No
        )

        if reply != QMessageBox.Yes:
            return

        # broadcast that the form is busy
        self.became_busy.emit(True, 'Removing an item...')

        # item you want to delete
        real_index = int(self.ui.table_items.item(self.ui.table_items.selectedItems()[0].row(), 6).text())
        item = self.items[real_index]

        # data_engine connections
        self.data_engine.response_received.connect(self.item_removed_received)
        self.data_engine.got_error.connect(self.server_error_received)

        # send a request to the server
        self.data_engine.send_request(
            url=const.REMOVE_ITEM_URL,
            params={'id': item.id},
        )

        # disable the remove button
        self.ui.button_remove.setEnabled(False)

    def clear_search_widget(self):
        """
        This method clears the line_search widget.
        :return: void
        """
        self.ui.line_search.clear()
        self.ui.table_items.clearSelection()

    def line_search_text_changed(self, filter_string=''):
        """
        This method will filter out the data inventory based on what the user
        inputs into the line_search widget
        :return: void
        """
        # make filter_string lowercase
        filter_string = str(filter_string).lower()

        # filter the table
        for i in range(self.ui.table_items.rowCount()):
            match = False

            for j in range(self.ui.table_items.columnCount()):
                if filter_string in str(self.ui.table_items.item(i, j).text()).lower():
                    match = True
                    break

            self.ui.table_items.setRowHidden(i, not match)

    def edit_item_dialog(self, row=None):
        """
        This is the slot for the button edit.  It will open a dialog so the user can edit an item form the
        inventory table.  It will pass in the inventory item form the items list.
        :return: void
        """
        if not row:
            row = self.ui.table_items.selectedItems()[0].row()

        real_index = int(self.ui.table_items.item(row, 6).text())
        selected_item = self.items[real_index]

        # setup dialog
        edit_item = InventoryItemDialog(
            data_engine=self.data_engine,
            units=self.units,
            classes=self.classes,
            item=selected_item,
            parent=self
        )
        # set up the connection
        # noinspection PyUnresolvedReferences
        edit_item.accepted.connect(self.request_items_list)

        # open the dialog
        edit_item.open()

#########################################################
#                 data engine slots                     #
#########################################################

    def items_list_received(self, response):
        """
        This method is the slot for the successful retrieval of an items form the server.
        The server sends back a dictionary, the connection is closed, and the list_items widget
        is updated with the items form the dictionary.
        :param response: Response from the server if not an error. A dictionary of items
        :return: void
        """
        # save a list of items
        # print(response)
        self.items = list(map(lambda current_item: Item(current_item), response['items']))
        self.units = sorted(set(response['units']))
        self.classes = sorted(set(response['item_classes']))

        # disconnect for the engine
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # broadcast that the form is not busy
        self.became_busy.emit(False, '')

        # report that we've got items now
        self.got_items_list.emit(self.items)

        # make the new table

        self.ui.table_items.setRowCount(len(self.items))

        # set items in the list
        for i, item in enumerate(self.items):
            nodes = [
                QTableWidgetItem(item.name),
                QTableWidgetItem(item.item_class),
                QTableWidgetItem(item.supplier),
                QTableWidgetItem(str(item.amount) + ' ' + item.units),
                QTableWidgetItem('$ ' + str(item.price)),
                QTableWidgetItem(str(item.extra)),
                QTableWidgetItem(str(i))
            ]

            # beginning of the workaround
            self.ui.table_items.setSortingEnabled(False)

            for j, node in enumerate(nodes):
                self.ui.table_items.setItem(i, j, node)
                if item.amount == 0:
                    node.setBackground(QColor('#FFC0CB'))
                elif item.amount <= item.reorder_amount:
                    node.setBackground(QColor('#F0DC82'))

            # end of workaround
            self.ui.table_items.setSortingEnabled(True)

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # broadcast that the form is no longer busy
        self.became_busy.emit(False, '')

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

    # noinspection PyUnusedLocal
    def item_added_to_database_received(self, response):
        """
        This method is the slot for the processed request to add an item into the database.
        if the request was successful, the method will close the connection and then refresh the
        list_item.
        :param response: The response from the server sent in the signal if the server
        did not run into a problem with adding an item.
        :return: void
        """
        # close connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # broadcast that the form is no longer busy
        self.became_busy.emit(False, '')

        # refresh items list
        self.request_items_list()

    # noinspection PyUnusedLocal
    def item_removed_received(self, response):
        """
        This method is the slot for the items removed signal if there isn't an error.
        It will close the connection then request that the
        :param response: the item removed, response from the server
        :return: void
        """
        # broadcast that the form is no longer busy
        self.became_busy.emit(False, '')

        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # clear the table selection
        self.ui.table_items.clearSelection()

        # update the list_items widget
        self.request_items_list()

#########################################################
#                 internal methods                      #
#########################################################

    def enable_button_remove(self, enable=True):
        """
        This is the method to enable to disable the button_remove object.
        :param enable: bool to enable or disable, dis
        :return: void
        """
        self.ui.button_remove.setEnabled(enable)

    def request_items_list(self):
        """
        This method gets the users entire inventory and passes it to the app so
        their inventory can be displayed in the list_items widget.
        :return: void
        """
        # data_engine connections
        self.data_engine.response_received.connect(self.items_list_received)
        self.data_engine.got_error.connect(self.server_error_received)

        # broadcast that the form is busy
        self.became_busy.emit(True, 'Updating your inventory...')

        # send a request to the server for an items dictionary
        self.data_engine.send_request(
            url=const.GET_ALL_ITEMS_URL,
            params={'sort_by': 'name'},
        )

    def remove_button_state(self):
        """
        This method sets botht he remove button and the edit button as enabled or disabled
        :return: void
        """
        self.ui.button_remove.setEnabled(len(self.ui.table_items.selectedItems()))
        self.ui.button_edit.setEnabled(len(self.ui.table_items.selectedItems()))

    def enable_form(self, busy):
        """
        This method sets the form enabled or disabled
        :param busy: bool, whether or not the form is busy
        :return: void
        """
        self.ui.groupBox.setEnabled(not busy)
