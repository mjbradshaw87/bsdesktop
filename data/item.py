# from random import randint, randrange, random


class Item(object):
    """
    This is the item class for every item in the users inventory.
    """
    def __init__(self, item_dict=None):
        """
        Constructor for the item
        :param item_dict: a dictionary of items to make into an item class
        :return: void
        """
        if not item_dict:
            item_dict = {}

        self.id = item_dict.get('id')
        self.item_class = item_dict.get('item_class')
        self.subclass = item_dict.get('item_subclass')
        self.name = item_dict.get('name')
        self.supplier = item_dict.get('supplier')
        self.amount = item_dict.get('amount')
        self.units = item_dict.get('unit')
        self.reorder_amount = item_dict.get('reorder_amount')
        self.has_to_reorder = False  # todo compare amount and reorder_amount
        self.extra = item_dict.get('extra_field')
        self.price = float(item_dict.get('price', 0.0))

    # @ classmethod
    # def generate_items(cls, qty=15):
    #     """
    #
    #     :param qty:
    #     :return:
    #     """
    #     # random sources
    #     class_names = ['Adjuncts', 'Brewers Aids', 'Chemicals', 'Hops', 'Malts']
    #     subclasses = ['Foam controls', 'Warts', 'Magnesium Sulfate', 'Sodium Chloride', 'Rice Hulls']
    #     names = ['Breweres Blue', 'Mag-on', 'Russian Red', 'Country Music', 'Superman', 'Texas Wowie']
    #     supplier = ['Ukraine', 'Belgium', 'Denver Grain', 'Midwest Supply']
    #     units = ['gal', 'g', 'lbs', 'oz', 'count']
    #
    #     final_items = []
    #
    #     for i in range(qty):
    #         # create a new item
    #         item = Item()
    #
    #         # fill new item with values
    #         item.id = randint(0, 999999)
    #         item.item_class = class_names[randint(0, len(class_names) - 1)]
    #         item.subclass = subclasses[randint(0, len(subclasses) - 1)]
    #         item.name = names[randint(0, len(names) - 1)] + str(i + 1)
    #         item.supplier = supplier[randint(0, len(supplier) - 1)]
    #         item.units = units[randint(0, len(units) - 1)]
    #         item.amount = random() * 1000
    #         item.reorder_amount = item.amount * 0.1
    #         item.has_to_reorder = bool(randint(0, 1))
    #
    #         # save it into a final list
    #         final_items.append(item)
    #
    #     return final_items
