import json
import os
import threading
from PyQt5.QtCore import QObject, pyqtSignal
import datetime
import requests
import sys
from data import const


def find_data_file(filename):
    """
    This method only serves a single puprose - to provide a working mac bundle paths
    :param filename: a file to return fullpath to
    :return: fullpath + filename
    """
    if getattr(sys, 'frozen', False):
        # The application is frozen
        datadir = os.path.dirname(sys.executable)
    else:
        # The application is not frozen
        # Change this bit to match where you store your data files:
        datadir = os.getcwd()

    return os.path.join(datadir, filename)


def format_datetime(timestamp, date_format=None):
    """

    :param timestamp:
    :param date_format:
    :return: the formated date time
    """
    if not timestamp:
        return ''

    if not date_format:
        date_format = '%x %I:%M %p'

    date = datetime.datetime.fromtimestamp(int(float(timestamp)))
    return date.strftime(date_format)


def threaded(func):
    """
    This decorator runs the decorated function in a separate thread, providing non-UI blocking methods
    :param func: A function/method to decorate
    :return: The decorated function
    """
    def wrapper(*args, **kwargs):
        threading.Thread(target=func, args=args, kwargs=kwargs).start()
    return wrapper


class DataEngine(QObject):
    """
    This is the data engine class.  This holds the methods to get data from the server.
    """

    response_received = pyqtSignal(['PyQt_PyObject'], name='response_received')
    user_does_not_exist = pyqtSignal()
    got_error = pyqtSignal(['QString'], name='got_error')

    def __init__(self, refresh_token=None):
        """
        This is the constructor for the DataEngine class.
        :param refresh_token:  This is the users refresh token.  This will allow the user
        to gain a new access token if the old one is expired or the user doesnt have one.
        :return: Class object of a DataEngine with user tokens.
        """
        super(DataEngine, self).__init__()

        self.should_stop = False

        # tokens
        self.refresh_token = refresh_token
        self.access_token = None

    def __update_access_token(self):
        """
        This is the private method to get a refresh access token.  If the user has an expired access
        token or none at all, then this will get one.  An outside method will call this
        and this will return a bool if the operation was successful or now.
        :return: Bool - if the server received a request and got a response.
        """
        response = requests.get(
            url=const.UPDATE_ACCESS_TOKEN_URL,
            params={'refresh_token': str(self.refresh_token)},
            verify=True
        )

        if response.status_code == requests.codes.ok:
            response_dict = json.loads(response.text)

            if response_dict.get('success'):
                self.access_token = response_dict['access_token']
                return True

            if response_dict['error_code'] == 5:
                self.user_does_not_exist.emit()
                self.should_stop = True

        return False

    @threaded
    def send_request(self, url=None, params=None, request_type='GET'):
        """
        This is a method to send a request to the server. The method will handle all requests to the server,
        and the thread is asyncronus, to allow the user to keep using the program and not have the
        program freeze on them.  If the user does not have a valid access token, the method will
        call an internal method to get a new access token.
        :param url: The URL of the request. Provided by the program based on the actions
        of the user in the UI.
        :param params: Any needed information for the server to complete the request.
        usually a dictionary.
        :param request_type: Depending on the request, this is either a GET or POST.
        :return: Nothing, a signal is sent if the operation was successful. or breaks if
        the operation was not.
        """
        # if there's no refresh token, can only send auth requests
        if not self.refresh_token:
            self.got_error.emit('No refresh token set up. Cannot make non-auth type requests.')
            return

        # count # of attempts to get a new access token
        attempts_count = 0

        # prepare params
        params = dict(map(lambda key_value: (key_value[0], str(key_value[1])), params.items()))

        while attempts_count < 3:
            # create the request_data object
            request_data = {
                'url': url,
                'params' if request_type == 'GET' else 'data': params,
                'headers': {'access-token': str(self.access_token)},
                'verify': True
            }

            if request_type == 'GET':
                response = requests.get(**request_data)
            else:
                response = requests.post(**request_data)

            if response.status_code == requests.codes.ok:
                response_dict = json.loads(response.text)

                if response_dict['success']:
                    self.response_received.emit(response_dict)
                    break
                else:
                    # check for access_token error
                    error_code = response_dict['error_code']

                    if error_code == 7 or error_code == 6:
                        if self.__update_access_token():
                            attempts_count += 1
                        else:
                            if self.should_stop:
                                return

                            self.got_error.emit('Unknown error')
                            break
                    else:
                        self.got_error.emit(response_dict['error'])
                        break
            else:
                self.got_error.emit(response.text)
                break

    @threaded
    def send_auth_request(self, url=None, params=None, request_type='GET'):
        """
        This is a method to send an auth request to the server. The method will handle auth requests to the server,
        and the thread is asyncronus, to allow the user to keep using the program and not have the
        program freeze on them.
        :param url: The URL of the request. Provided by the program based on the actions
        of the user in the UI.
        :param params: Any needed information for the server to complete the request.
        usually a dictionary.
        :param request_type: Depending on the request, this is either a GET or POST.
        :return: void
        """
        # prepare params
        params = dict(map(lambda key_value: (key_value[0], str(key_value[1])), params.items()))

        # create the request_data object
        request_data = {
            'url': url,
            'params' if request_type == 'GET' else 'data': params,
            'verify': True
        }

        if request_type == 'GET':
            response = requests.get(**request_data)
        else:
            response = requests.post(**request_data)

        if response.status_code == requests.codes.ok:
            response_dict = json.loads(response.text)

            if response_dict['success']:
                self.response_received.emit(response_dict)
            else:
                self.got_error.emit(response_dict['error'])
        else:
            self.got_error.emit(response.text)
