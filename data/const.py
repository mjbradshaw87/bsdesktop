

########################################################
#                     Controls                         #
########################################################

DEBUG = False

APP_NAME = 'BeerScribe'
APP_VERSION = '1.0'
DOMAIN = 'https://beerscribe.biz/' if not DEBUG else 'http://localhost:8000/'
API_V1 = 'api/v1/'


########################################################
#                    constants                         #
########################################################

# URLS
# user
UPDATE_ACCESS_TOKEN_URL = DOMAIN + API_V1 + 'user/update-access-token'
GET_USER_INFO_URL = DOMAIN + API_V1 + 'user/info'
REGISTER_USER_URL = DOMAIN + API_V1 + 'user/register'

# items
ADD_ITEM_INTO_DATABASE_URL = DOMAIN + API_V1 + 'item/add'
EDIT_ITEM_INTO_DATABASE_URL = DOMAIN + API_V1 + 'item/edit'
GET_ALL_ITEMS_URL = DOMAIN + API_V1 + 'item/all'
REMOVE_ITEM_URL = DOMAIN + API_V1 + 'item/delete'

# recipes
ADD_RECIPE_INTO_DATABASE_URL = DOMAIN + API_V1 + 'recipe/add'
EDIT_RECIPE_INTO_DATABASE_URL = DOMAIN + API_V1 + 'recipe/edit'
GET_ALL_RECIPES_URL = DOMAIN + API_V1 + 'recipe/all'
REMOVE_RECIPE_URL = DOMAIN + API_V1 + 'recipe/delete'

# brews
ADD_BREW_URL = DOMAIN + API_V1 + 'brew/add'
ALL_BREWS_URL = DOMAIN + API_V1 + 'brew/all'
EDIT_BREW_URL = DOMAIN + API_V1 + 'brew/edit'

# mash
ADD_MASH_INFO_URL = DOMAIN + API_V1 + 'brew/mash-info/add'
EDIT_MASH_INFO_URL = DOMAIN + API_V1 + 'brew/mash-info/edit'

# lauter
ADD_LAUTER_INFO_URL = DOMAIN + API_V1 + 'brew/lauter-info/add'
EDIT_LAUTER_INFO_URL = DOMAIN + API_V1 + 'brew/lauter-info/edit'

# kettle
ADD_KETTLE_INFO_URL = DOMAIN + API_V1 + 'brew/kettle-info/add'
EDIT_KETTLE_INFO_URL = DOMAIN + API_V1 + 'brew/kettle-info/edit'

# wort
ADD_WORT_INFO_URL = DOMAIN + API_V1 + 'brew/wort-info/add'
EDIT_WORT_INFO_URL = DOMAIN + API_V1 + 'brew/wort-info/edit'

# cellar
ADD_CELLAR_INFO_URL = DOMAIN + API_V1 + 'brew/cellar-info/add'
EDIT_CELLAR_INFO_URL = DOMAIN + API_V1 + 'brew/cellar-info/edit'

# notes
ADD_NOTE_URL = DOMAIN + API_V1 + 'brew/note/add'
EDIT_NOTE_URL = DOMAIN + API_V1 + 'brew/note/edit'
