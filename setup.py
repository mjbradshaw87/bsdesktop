"""
This is a setup.py for building binaries

Usage:
    windows build:
        python setup.py build

    windows installer:
        python setup.py bdist_msi

    mac build (use this mainly!):
        python3 setup.py bdist_dmg

    after that, copy data/qt.conf file into the built package:
    <name_of_package>.app/Contents/Resources

    if you see something like this: ImportError: No module named 'PyQt5.uic.port_v2.invoke'
    then remove port_v2 folder from here (the path may vary but you'll find it):

        mac:     /usr/local/Cellar/pyqt5/<some_number_here>/lib/python3.4/site-packages/PyQt5/uic/
        windows: C:/Python34/site-packages/PyQt5/uic/

    if you see something like this: ImportError: No module named 'PyQt5.uic.port_v2.invoke'
    then remove port_v2 folder from here (the path may vary but you'll find it):

        windows: C:/Python34/site-packages/PyQt5/uic/
"""

from cx_Freeze import setup, Executable
import sys
import requests.certs


app_name = 'bsdesktop'
base = None


if sys.platform == 'win32':
    app_name += '.exe'
    base = 'Win32GUI'


# prepare executable
executables = [
    Executable(
        script='main.py', 
        base=base, 
        compress=True, 
        targetName=app_name,
        copyDependentFiles=True,
        # shortcutName='BeerScribe',
        # shortcutDir='DesktopFolder'
    )
]


# add building options
build_options = {
    'packages': [
        'sip', 'PyQt5.QtCore', 'PyQt5.QtGui', 'PyQt5.QtWebKit',
        'PyQt5.QtPrintSupport', 'requests', 'jinja2', 'jinja2.ext'
    ],
    'include_msvcr': True,
    'excludes': ['tkinter'],
    'include_files': ['html_forms/', (requests.certs.where(), 'cacert.pem')],
    # 'includes': ['atexit']
}


# creating a desktop shortcut for Windows
shortcut_table = [
    (
        'BeerScribe',                # Shortcut
        'DesktopFolder',             # Directory_
        'BeerScribe',                # Name
        'TARGETDIR',                 # Component_
        '[TARGETDIR]bsdesktop.exe',  # Target
        None,                        # Arguments
        None,                        # Description
        None,                        # Hotkey
        None,                        # Icon
        None,                        # IconIndex
        None,                        # ShowCmd
        'TARGETDIR'                  # WkDir
    ),
    # [
    #     'BeerScribe',                # Shortcut
    #     'StartMenuFolder',            # Directory_
    #     'BeerScribe',                # Name
    #     'TARGETDIR',                 # Component_
    #     '[TARGETDIR]bsdesktop.exe',  # Target
    #     None,                        # Arguments
    #     None,                        # Description
    #     None,                        # Hotkey
    #     None,                        # Icon
    #     None,                        # IconIndex
    #     None,                        # ShowCmd
    #     'TARGETDIR'                  # WkDir
    # ]
]


# setup the build process
setup(
    name='BeerScribe',
    version='1.0',
    description='BeerScribe, the managing app for small breweries',
    options={
        'bdist_msi': {'data': {'Shortcut': shortcut_table}},
        'build_exe': build_options
    },
    executables=executables,
)
