# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_notes_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_NotesDialog(object):
    def setupUi(self, NotesDialog):
        NotesDialog.setObjectName("NotesDialog")
        NotesDialog.resize(424, 279)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(NotesDialog)
        self.verticalLayout_2.setContentsMargins(3, 3, 3, 3)
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.group_note = QtWidgets.QGroupBox(NotesDialog)
        self.group_note.setObjectName("group_note")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.group_note)
        self.verticalLayout.setContentsMargins(3, 3, 3, 3)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(self.group_note)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.line_note_title = QtWidgets.QLineEdit(self.group_note)
        self.line_note_title.setObjectName("line_note_title")
        self.horizontalLayout_2.addWidget(self.line_note_title)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.label_count = QtWidgets.QLabel(self.group_note)
        self.label_count.setText("")
        self.label_count.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_count.setObjectName("label_count")
        self.horizontalLayout_2.addWidget(self.label_count)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.text_note = QtWidgets.QPlainTextEdit(self.group_note)
        self.text_note.setObjectName("text_note")
        self.horizontalLayout_3.addWidget(self.text_note)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.verticalLayout_2.addWidget(self.group_note)
        self.layout_status = QtWidgets.QHBoxLayout()
        self.layout_status.setObjectName("layout_status")
        self.label_status = QtWidgets.QLabel(NotesDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_status.sizePolicy().hasHeightForWidth())
        self.label_status.setSizePolicy(sizePolicy)
        self.label_status.setText("")
        self.label_status.setObjectName("label_status")
        self.layout_status.addWidget(self.label_status)
        self.button_box = QtWidgets.QDialogButtonBox(NotesDialog)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.layout_status.addWidget(self.button_box)
        self.verticalLayout_2.addLayout(self.layout_status)

        self.retranslateUi(NotesDialog)
        QtCore.QMetaObject.connectSlotsByName(NotesDialog)

    def retranslateUi(self, NotesDialog):
        _translate = QtCore.QCoreApplication.translate
        NotesDialog.setWindowTitle(_translate("NotesDialog", "Edit Note"))
        self.label.setText(_translate("NotesDialog", "Title:"))
        self.text_note.setPlaceholderText(_translate("NotesDialog", "Write note here..."))

