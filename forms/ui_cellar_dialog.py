# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_cellar_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CellarDialog(object):
    def setupUi(self, CellarDialog):
        CellarDialog.setObjectName("CellarDialog")
        CellarDialog.resize(411, 153)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(CellarDialog)
        self.verticalLayout_3.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.group_cellar = QtWidgets.QGroupBox(CellarDialog)
        self.group_cellar.setObjectName("group_cellar")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.group_cellar)
        self.horizontalLayout.setContentsMargins(6, 6, 6, 6)
        self.horizontalLayout.setSpacing(3)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.group_cellar)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(self.group_cellar)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtWidgets.QLabel(self.group_cellar)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.check_present = QtWidgets.QCheckBox(self.group_cellar)
        self.check_present.setObjectName("check_present")
        self.verticalLayout_2.addWidget(self.check_present)
        self.cellar_blow_off = QtWidgets.QDateTimeEdit(self.group_cellar)
        self.cellar_blow_off.setCalendarPopup(True)
        self.cellar_blow_off.setObjectName("cellar_blow_off")
        self.verticalLayout_2.addWidget(self.cellar_blow_off)
        self.double_yeast = QtWidgets.QDoubleSpinBox(self.group_cellar)
        self.double_yeast.setObjectName("double_yeast")
        self.verticalLayout_2.addWidget(self.double_yeast)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout_3.addWidget(self.group_cellar)
        self.layout_status = QtWidgets.QHBoxLayout()
        self.layout_status.setObjectName("layout_status")
        self.label_status = QtWidgets.QLabel(CellarDialog)
        self.label_status.setText("")
        self.label_status.setObjectName("label_status")
        self.layout_status.addWidget(self.label_status)
        self.button_box = QtWidgets.QDialogButtonBox(CellarDialog)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.layout_status.addWidget(self.button_box)
        self.verticalLayout_3.addLayout(self.layout_status)

        self.retranslateUi(CellarDialog)
        QtCore.QMetaObject.connectSlotsByName(CellarDialog)

    def retranslateUi(self, CellarDialog):
        _translate = QtCore.QCoreApplication.translate
        CellarDialog.setWindowTitle(_translate("CellarDialog", "Cellar"))
        self.label.setText(_translate("CellarDialog", "Glycol:"))
        self.label_2.setText(_translate("CellarDialog", "Blow off Attachment:"))
        self.label_3.setText(_translate("CellarDialog", "Yeast Type:"))
        self.check_present.setText(_translate("CellarDialog", "Present"))

