# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_bbl_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_BblDialog(object):
    def setupUi(self, BblDialog):
        BblDialog.setObjectName("BblDialog")
        BblDialog.resize(321, 156)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(BblDialog)
        self.verticalLayout_3.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout_3.setSpacing(3)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.group_info = QtWidgets.QGroupBox(BblDialog)
        self.group_info.setObjectName("group_info")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.group_info)
        self.horizontalLayout.setContentsMargins(6, 6, 6, 6)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.group_info)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(self.group_info)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtWidgets.QLabel(self.group_info)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.double_start = QtWidgets.QDoubleSpinBox(self.group_info)
        self.double_start.setObjectName("double_start")
        self.verticalLayout_2.addWidget(self.double_start)
        self.line_change = QtWidgets.QLineEdit(self.group_info)
        self.line_change.setEnabled(False)
        self.line_change.setObjectName("line_change")
        self.verticalLayout_2.addWidget(self.line_change)
        self.double_end = QtWidgets.QDoubleSpinBox(self.group_info)
        self.double_end.setObjectName("double_end")
        self.verticalLayout_2.addWidget(self.double_end)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout_3.addWidget(self.group_info)
        self.layout_status = QtWidgets.QHBoxLayout()
        self.layout_status.setContentsMargins(0, 0, 0, -1)
        self.layout_status.setSpacing(3)
        self.layout_status.setObjectName("layout_status")
        self.label_status = QtWidgets.QLabel(BblDialog)
        self.label_status.setObjectName("label_status")
        self.layout_status.addWidget(self.label_status)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.layout_status.addItem(spacerItem2)
        self.button_box = QtWidgets.QDialogButtonBox(BblDialog)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.layout_status.addWidget(self.button_box)
        self.verticalLayout_3.addLayout(self.layout_status)

        self.retranslateUi(BblDialog)
        QtCore.QMetaObject.connectSlotsByName(BblDialog)

    def retranslateUi(self, BblDialog):
        _translate = QtCore.QCoreApplication.translate
        BblDialog.setWindowTitle(_translate("BblDialog", "Dialog"))
        self.label.setText(_translate("BblDialog", "Start BBL:"))
        self.label_2.setText(_translate("BblDialog", "Change:"))
        self.label_3.setText(_translate("BblDialog", "End BBL:"))
        self.label_status.setText(_translate("BblDialog", "TextLabel"))

