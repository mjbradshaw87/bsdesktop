# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_inventory_control_widget.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_InventoryControlWidget(object):
    def setupUi(self, InventoryControlWidget):
        InventoryControlWidget.setObjectName("InventoryControlWidget")
        InventoryControlWidget.setWindowModality(QtCore.Qt.NonModal)
        InventoryControlWidget.setEnabled(True)
        InventoryControlWidget.resize(683, 517)
        self.verticalLayout = QtWidgets.QVBoxLayout(InventoryControlWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(InventoryControlWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setContentsMargins(6, 3, 6, 3)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.button_add = QtWidgets.QPushButton(self.groupBox)
        self.button_add.setObjectName("button_add")
        self.horizontalLayout_3.addWidget(self.button_add)
        self.button_edit = QtWidgets.QPushButton(self.groupBox)
        self.button_edit.setObjectName("button_edit")
        self.horizontalLayout_3.addWidget(self.button_edit)
        self.button_remove = QtWidgets.QPushButton(self.groupBox)
        self.button_remove.setObjectName("button_remove")
        self.horizontalLayout_3.addWidget(self.button_remove)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.horizontalLayout_4.addLayout(self.horizontalLayout_3)
        self.line_search = QtWidgets.QLineEdit(self.groupBox)
        self.line_search.setClearButtonEnabled(True)
        self.line_search.setObjectName("line_search")
        self.horizontalLayout_4.addWidget(self.line_search)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.table_items = QtWidgets.QTableWidget(self.groupBox)
        self.table_items.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.table_items.setAlternatingRowColors(False)
        self.table_items.setObjectName("table_items")
        self.table_items.setColumnCount(0)
        self.table_items.setRowCount(0)
        self.verticalLayout_2.addWidget(self.table_items)
        self.verticalLayout.addWidget(self.groupBox)

        self.retranslateUi(InventoryControlWidget)
        QtCore.QMetaObject.connectSlotsByName(InventoryControlWidget)
        InventoryControlWidget.setTabOrder(self.line_search, self.table_items)
        InventoryControlWidget.setTabOrder(self.table_items, self.button_remove)

    def retranslateUi(self, InventoryControlWidget):
        _translate = QtCore.QCoreApplication.translate
        InventoryControlWidget.setWindowTitle(_translate("InventoryControlWidget", "Form"))
        self.groupBox.setTitle(_translate("InventoryControlWidget", "Items"))
        self.button_add.setText(_translate("InventoryControlWidget", "Add..."))
        self.button_add.setShortcut(_translate("InventoryControlWidget", "Ctrl+A"))
        self.button_edit.setText(_translate("InventoryControlWidget", "View/Edit..."))
        self.button_remove.setText(_translate("InventoryControlWidget", "Remove"))
        self.line_search.setPlaceholderText(_translate("InventoryControlWidget", "Enter the Item that you are looking for"))
        self.table_items.setSortingEnabled(True)

