# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_wort_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WortDialog(object):
    def setupUi(self, WortDialog):
        WortDialog.setObjectName("WortDialog")
        WortDialog.resize(301, 159)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(WortDialog)
        self.verticalLayout_3.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout_3.setSpacing(3)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.group_wort = QtWidgets.QGroupBox(WortDialog)
        self.group_wort.setObjectName("group_wort")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.group_wort)
        self.horizontalLayout.setContentsMargins(6, 6, 6, 6)
        self.horizontalLayout.setSpacing(3)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_3 = QtWidgets.QLabel(self.group_wort)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.label_2 = QtWidgets.QLabel(self.group_wort)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label = QtWidgets.QLabel(self.group_wort)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.double_wort_volume = QtWidgets.QDoubleSpinBox(self.group_wort)
        self.double_wort_volume.setObjectName("double_wort_volume")
        self.verticalLayout_2.addWidget(self.double_wort_volume)
        self.double_og = QtWidgets.QDoubleSpinBox(self.group_wort)
        self.double_og.setObjectName("double_og")
        self.verticalLayout_2.addWidget(self.double_og)
        self.double_color_srm = QtWidgets.QDoubleSpinBox(self.group_wort)
        self.double_color_srm.setObjectName("double_color_srm")
        self.verticalLayout_2.addWidget(self.double_color_srm)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout_3.addWidget(self.group_wort)
        self.layout_status = QtWidgets.QHBoxLayout()
        self.layout_status.setObjectName("layout_status")
        self.label_status = QtWidgets.QLabel(WortDialog)
        self.label_status.setText("")
        self.label_status.setObjectName("label_status")
        self.layout_status.addWidget(self.label_status)
        self.button_box = QtWidgets.QDialogButtonBox(WortDialog)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.layout_status.addWidget(self.button_box)
        self.verticalLayout_3.addLayout(self.layout_status)

        self.retranslateUi(WortDialog)
        QtCore.QMetaObject.connectSlotsByName(WortDialog)

    def retranslateUi(self, WortDialog):
        _translate = QtCore.QCoreApplication.translate
        WortDialog.setWindowTitle(_translate("WortDialog", "Wort"))
        self.label_3.setText(_translate("WortDialog", "Wort Volume BBL:"))
        self.label_2.setText(_translate("WortDialog", "OG: (°P):"))
        self.label.setText(_translate("WortDialog", "Color SRM:"))

