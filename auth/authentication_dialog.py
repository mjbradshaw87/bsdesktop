from PyQt5.QtWidgets import QStackedWidget, QMessageBox, QDialog, QVBoxLayout
from .phone_number_widget import PhoneNumberWidget
from .pin_number_widget import PinNumberWidget
from .new_user_setup_widget import NewUserSetupWidget


class AuthenticationDialog(QDialog):
    """
    This is the over-aching dialog to control all of BeerScribe's authentication process.
    """
    def __init__(self, parent=None):
        """
        Constructor for the AuthenticationDialog
        :return:
        """
        super(AuthenticationDialog, self).__init__(parent)
        self.setModal(True)

        # set the window title
        self.stacked_widget = QStackedWidget()
        self.setWindowTitle('Authentication')

        # set up child widgets and their slots
        self.phone_number_widget = PhoneNumberWidget()
        self.stacked_widget.addWidget(self.phone_number_widget)
        self.phone_number_widget.got_error.connect(self.phone_number_widget_got_error)
        self.phone_number_widget.verification_id_received.connect(self.verification_id_received)

        self.pin_number_widget = None
        self.new_user_widget = None

        # variables
        self.user_info = None

        # save the stack
        main_layout = QVBoxLayout()
        main_layout.addWidget(self.stacked_widget)
        self.setLayout(main_layout)

        # setup the widget itself
        self.resize(600, 500)

    def closeEvent(self, event):
        # exit message for the user
        exit_message = 'You have to authorize in order to use BeerScribe.\n' \
                       'Are you sure you want to quit?'

        # set up the QMessageBox to prompt the user
        exit_box = QMessageBox(self)
        exit_box.setText(exit_message)

        # ask the user if they want to exit the program
        reply = exit_box.question(
            self,
            'Exit Confirmation',
            exit_message,
            QMessageBox.Yes,
            QMessageBox.No
        )

        # Action of the QMessageBox.  Depends on the user input.  If the user selects to exit
        # the app, the save settings method will be called.  If the user chooses to ignore
        # the event, it will do nothing.
        event.accept() if (reply == QMessageBox.Yes) else event.ignore()

    def phone_number_widget_got_error(self, message):
        """
        This method sends up an error message box with the error code if beerscribe cannot
        reach the server or there was an error with the number.  If you reach this message,
        then check the URL to make sure the program is going to the right address and to see if
        the program is getting internet access.
        :param message: The error message from the server.
        :return: Nothing
        """
        box = QMessageBox()
        box.setText(message)
        box.exec_()

    def verification_id_received(self, verification_id):
        """
        This method is the slot for catching the verification_id_received signal from the
        phone number widget..
        When this method is called, the phone number widget received a response from
        the server and it calls the pin number widget.
        :param verification_id: This is the verification ID that comes from the server.
        :return: Nothing
        """
        self.pin_number_widget = PinNumberWidget(parent=self, verification_id=verification_id)

        self.pin_number_widget.got_error.connect(self.phone_number_widget_got_error)
        self.pin_number_widget.credentials_received.connect(self.credentials_received)
        self.pin_number_widget.ticket_received.connect(self.ticket_received)
        self.pin_number_widget.back_button_clicked.connect(self.pin_code_widget_back_button_clicked)

        self.stacked_widget.addWidget(self.pin_number_widget)
        self.stacked_widget.setCurrentWidget(self.pin_number_widget)

    def credentials_received(self):
        """
        This method is the slot for the credentials_received signal from
        pin number widget.
        When this method is called, the pin_number_widget has received a response from the
        server and the user has an account.  It sends the user right to the app with the tokens.
        :return: Nothing
        """
        if self.sender() == self.pin_number_widget:
            self.user_info = self.pin_number_widget.user_info
        else:
            self.user_info = self.new_user_widget.user_info

        self.accept()

    def ticket_received(self, ticket_id):
        """
        This method is the slot for the ticket_received signal from the pin number widget.
        When this is called, the pin number widget has received a signal from the server, but
        the user is new.  This will call the new user form.
        :param ticket_id: This is the ticket for the new user form to register the new user.
        :return: Nothing
        """
        self.new_user_widget = NewUserSetupWidget(
            ticket_id=ticket_id,
            phone=self.phone_number_widget.phone,
            parent=self
        )
        self.stacked_widget.addWidget(self.new_user_widget)
        self.stacked_widget.setCurrentWidget(self.new_user_widget)

        self.new_user_widget.back_button_clicked.connect(self.new_user_back_button_clicked)
        self.new_user_widget.credentials_received.connect(self.credentials_received)
        self.new_user_widget.got_error.connect(self.phone_number_widget_got_error)

    def pin_code_widget_back_button_clicked(self):
        """
        This is the slot when the back button is clicked on the pin code widget.
        This method removes the PinNumberWidget form the widget stack.
        :return: Nothing
        """
        self.stacked_widget.removeWidget(self.pin_number_widget)
        self.pin_number_widget = None

    def new_user_back_button_clicked(self):
        """
        This is the slot when the new user form back button is clicked.
        this method removes the NewUserWidget form the widget stack.
        :return: Nothing
        """
        self.stacked_widget.removeWidget(self.new_user_widget)
        self.new_user_widget = None

        self.stacked_widget.removeWidget(self.pin_number_widget)
        self.pin_number_widget = None

        self.stacked_widget.setCurrentIndex(0)
