import json
import requests
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QIntValidator
from .ui_pin_number_widget import Ui_PinNumberWidget
from custom_controls.qprogressindicator import QProgressIndicator
from data.data_engine import threaded
from .const import CHECK_PIN_CODE_URL, PIN_CODE_LENGTH


class PinNumberWidget(QWidget):
    """
    This is the class for the PinNumberWidget
    This class will take a PIN input that the user gets
    on their phone, and with that number it will take the PIN and the
    authentication ID, then it will get tokens for the app,
    if the user exists or permission to create a user if the number does not exist.
    """
    back_button_clicked = pyqtSignal()
    credentials_received = pyqtSignal()
    ticket_received = pyqtSignal(['QString'], name='ticket_received')
    got_error = pyqtSignal(['QString'], name='got_error')

    # internal signals
    __pin_check_error = pyqtSignal(['QString'], name='pin_check_error')
    __check_finished = pyqtSignal()

    def showEvent(self, event):
        """
        This method forces focus on the phone number field
        :param event: event object
        :return: void
        """
        self.ui.line_pin_number.selectAll()

        if not self.ui.line_pin_number.hasFocus():
            # set phone number field in focus
            self.ui.line_pin_number.setFocus(Qt.OtherFocusReason)

    def hideEvent(self, event):
        """
        This method clears the focus from the pin field (fixing a strange mac bug)
        :param event: event object
        :return: void
        """
        self.ui.line_pin_number.clearFocus()

    def __init__(self, verification_id, signin_mode=False, parent=None):
        super(PinNumberWidget, self).__init__(parent)

        self.ui = Ui_PinNumberWidget()
        self.ui.setupUi(self)

        # set up variables
        self.verification_id = verification_id
        self.signin_mode = signin_mode
        self.user_info = None
        self.progress = QProgressIndicator(self)

        # set up behaviors
        self.ui.line_pin_number.setValidator(QIntValidator(0, 99999))
        self.ui.label_error.setVisible(False)
        self.ui.layout_progress.addWidget(self.progress)

        # set up signals
        self.__pin_check_error.connect(self.check_finished_with_error)
        self.__check_finished.connect(self.check_finished)
        self.ui.line_pin_number.textChanged.connect(self.line_pin_number_text_changed)
        self.ui.button_back.clicked.connect(self.button_back_clicked)

    def line_pin_number_text_changed(self):
        """
        This method is the slot for the line_pin_number.
        When the length of the pin text is 5, the sign in button will enable and the user
        will be allowed to sign in to the app or get the new user signup.
        :return: void
        """
        # getting pin code
        pin_code = self.ui.line_pin_number.text()

        # enough digits for the pin code check
        if len(pin_code) == PIN_CODE_LENGTH:
            # getting ready to perform the request
            self.ui.line_pin_number.setEnabled(False)
            self.ui.label_error.setVisible(False)

            # lock form
            self.switch_load_mode()

            # run the check
            self.__check_pin_code(pin_code)

    @threaded
    def __check_pin_code(self, pin_code):
        """
        This method checks the pin code from the line_pin_number.
        will be allowed to sign in to the app or get the new user signup.
        :return: void
        """
        response = requests.get(
            url=CHECK_PIN_CODE_URL,
            params={
                'verification_id': str(self.verification_id),
                'pin': str(pin_code)
            }
        )

        if response.status_code == requests.codes.ok:
            response_dict = json.loads(response.text)

            if response_dict['success']:
                if response_dict['user_exists']:
                    self.user_info = {
                        'user': response_dict['user'],
                        'tokens': response_dict['tokens']
                    }

                    self.credentials_received.emit()
                else:
                    self.user_info = None
                    self.ui.line_pin_number.clear()
                    self.ui.line_pin_number.setAttribute(Qt.WA_MacShowFocusRect, False)
                    self.ticket_received.emit(response_dict['ticket_id'])
            else:
                self.pin_check_error.emit('<font color="red">' + response_dict['error'] + '</font>')
        else:
            self.pin_check_error.emit('<font color="red">Connection error. Try again</font>')

        # the check is complete
        self.__check_finished.emit()

    def check_finished_with_error(self, error_text=''):
        """
        This slot will be called if the pin code check finished with an error
        :param error_text: the text to show in the error label
        :return: void
        """
        self.ui.line_pin_number.clear()
        self.ui.label_error.setText(error_text)
        self.ui.label_error.setVisible(True)
        self.user_info = None

    def check_finished(self):
        """
        This slot is being called every time the pin code check method has been completed
        :return: void
        """
        # unlock the form again
        self.ui.line_pin_number.setEnabled(True)

        # unlock form
        self.switch_load_mode(False)

    def button_back_clicked(self):
        """
        This is the action for when the button_sign is clicked.
        When the button is clicked, there will be a request sent to the server to get a pin code.
        :return:
        """
        self.back_button_clicked.emit()

    def switch_load_mode(self, load_mode=True):
        """
        This is the action for the pin number widget to disable the form and show a
        progress indicator widget.
        :param load_mode: bool, if the form is getting info from the server.
        :return:
        """
        self.ui.line_pin_number.setEnabled(not load_mode)
        self.ui.button_back.setEnabled(not load_mode)
        self.progress.startAnimation() if load_mode else self.progress.stopAnimation()
