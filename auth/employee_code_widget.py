from PyQt5.QtWidgets import QWidget
from .ui_employee_code_widget import Ui_EmployeeCodeWidget


class EmployeeCodeWidget(QWidget):
    """
    This is the employee code widget class.  This is the form to input the employee code to add a user to
    and existing company.
    """
    # noinspection PyUnresolvedReferences
    def __init__(self):
        """
        This is the constructor for the employee code class.
        :return: Class Object for the EmployeeCodeWidget.
        """
        super(EmployeeCodeWidget, self).__init__()

        # set up from
        self.ui = Ui_EmployeeCodeWidget()
        self.ui.setupUi(self)

        # variables
        self.pin_length = None

        # slots
        self.ui.button_back.clicked.connect(self.button_back_clicked)
        self.ui.line_employee_code.textChanged.connect(self.line_employee_code_value_changed)

    def line_employee_code_value_changed(self):
        """
        This method is the method to determine if the value of the
        line_employee_code_widget has 10 digits.  It will broadcast a signal if
        the len = 10.
        :return: Nothing
        """
        pass

    def button_back_clicked(self):
        """
        This is the action for the back button.  When clicked, this button will broadcast a signal
        to the auth method and check the employee code.
        :return: Nothing
        """
        pass
