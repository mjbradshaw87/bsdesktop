import json
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtWidgets import QWidget
import requests
from .ui_new_user_setup_widget import Ui_NewUserSetupWidget
from .const import REGISTER_USER_URL
from data.data_engine import threaded
from custom_controls.qprogressindicator import QProgressIndicator


class NewUserSetupWidget(QWidget):
    """
    This class is the controller for the ui_new_user_info_widget form.
    This class will take a new users info if they do not have an id in our servers,
    it will have the necessary fields to add someone into our system.
    """
    # signals
    back_button_clicked = pyqtSignal()
    credentials_received = pyqtSignal()
    got_error = pyqtSignal(['QString'], name='got_error')

    def __init__(self, ticket_id, phone, parent=None):
        """
        The constructor for the NewUserSetupWidget.  Takes two parameters.
        :param ticket_id:
        :param phone: The phone number of the user, this is from the phone number widget
        and sent into the constructor from the authentication widget.
        :return: A class object
        """
        super(NewUserSetupWidget, self).__init__(parent)

        # set up the form
        self.ui = Ui_NewUserSetupWidget()
        self.ui.setupUi(self)

        # variables
        self.phone = phone
        self.ticket_id = ticket_id
        self.user_info = None
        self.progress = QProgressIndicator(self)

        # behaviors
        self.ui.button_register.setEnabled(False)
        self.ui.line_phone.setText(self.phone)
        self.ui.line_phone.setReadOnly(True)
        self.ui.layout_activity.addWidget(self.progress)

        # Slots
        self.ui.button_register.clicked.connect(self.button_register_clicked)
        self.ui.button_back.clicked.connect(self.button_back_clicked)
        self.ui.line_last_name.textChanged.connect(self.should_enable_register_button)
        self.ui.line_first_name.textChanged.connect(self.should_enable_register_button)
        self.ui.line_email.textChanged.connect(self.should_enable_register_button)

    def showEvent(self, event):
        """
        This method forces focus on the phone number field
        :param event:
        :return:
        """
        self.ui.line_last_name.selectAll()

        if not self.ui.line_last_name.hasFocus():
            # set phone number field in focus
            self.ui.line_last_name.setFocus(Qt.OtherFocusReason)

    def button_back_clicked(self):
        """
        This is the action for the back button.  When clicked this method will emit a signal to the auth
        widget, it will move the form back to the phone number widget.
        :return: Nothing
        """
        self.back_button_clicked.emit()

    def should_enable_register_button(self):
        """
        This is the method for the register button.  This method will determine when the register button
        should be enabled.
        :return: Nothing
        """
        self.ui.button_register.setEnabled(
            len(self.ui.line_last_name.text()) and
            len(self.ui.line_first_name.text()) and
            len(self.ui.line_email.text())
        )

    # noinspection PyUnusedLocal
    @threaded
    def button_register_clicked(self, nonsense=None):
        """
        This is the action for the register button. When clicked, this method
        will first lock the form, it will then send a request to the server.
        The server will send the information if the user is not in the
        system.  After, it will unlock the form.
        :return: Nothing
        """
        self.lock_the_form()

        response = requests.post(
            url=REGISTER_USER_URL,
            data={
                'last_name': str(self.ui.line_last_name.text()),
                'first_name': str(self.ui.line_first_name.text()),
                'email': str(self.ui.line_first_name.text()),
                'ticket_id': str(self.ticket_id)
            }
        )

        if response.status_code == requests.codes.ok:
            response_dict = json.loads(response.text)

            # if true, we created a new user
            if response_dict['success']:
                self.user_info = {
                    'user': response_dict['user'],
                    'tokens': response_dict['tokens']
                }
                self.credentials_received.emit()

            # else, we did not create a new user, something went wrong
            else:
                self.user_info = None
                self.got_error.emit(response_dict['error'])
        else:
            self.user_info = None
            self.got_error.emit('Connection Error. Try again')

        self.lock_the_form(True)

    def lock_the_form(self, lock=False):
        """
        This is the method to lock or unlock the form.  It is used when the register button is clicked.
        :param lock: This is a bool value to lock or unlock the form.
        :return: Nothing
        """
        self.progress.startAnimation() if not lock else self.progress.stopAnimation()
        self.ui.line_last_name.setEnabled(lock)
        self.ui.line_first_name.setEnabled(lock)
        self.ui.line_email.setEnabled(lock)
        self.ui.button_register.setEnabled(lock)
