########################################################
#                    constants                         #
########################################################

DEBUG = False

PIN_CODE_LENGTH = 5

DOMAIN = 'https://beerscribe.biz/' if not DEBUG else 'http://localhost:8000/'
API_V1 = 'api/v1/'


# URLS
GET_PIN_CODE_URL = DOMAIN + API_V1 + 'auth/send-pin-code'
CHECK_PIN_CODE_URL = DOMAIN + API_V1 + 'auth/check-pin-code'
REGISTER_USER_URL = DOMAIN + API_V1 + 'user/register'
