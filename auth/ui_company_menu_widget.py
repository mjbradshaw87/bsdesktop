# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_company_menu_widget.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CompanyMenuWidget(object):
    def setupUi(self, CompanyMenuWidget):
        CompanyMenuWidget.setObjectName("CompanyMenuWidget")
        CompanyMenuWidget.resize(546, 158)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(CompanyMenuWidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(103, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.button_new_company = QtWidgets.QPushButton(CompanyMenuWidget)
        self.button_new_company.setObjectName("button_new_company")
        self.horizontalLayout.addWidget(self.button_new_company)
        self.button_existing_company = QtWidgets.QPushButton(CompanyMenuWidget)
        self.button_existing_company.setObjectName("button_existing_company")
        self.horizontalLayout.addWidget(self.button_existing_company)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        spacerItem3 = QtWidgets.QSpacerItem(102, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)

        self.retranslateUi(CompanyMenuWidget)
        QtCore.QMetaObject.connectSlotsByName(CompanyMenuWidget)

    def retranslateUi(self, CompanyMenuWidget):
        _translate = QtCore.QCoreApplication.translate
        CompanyMenuWidget.setWindowTitle(_translate("CompanyMenuWidget", "Form"))
        self.button_new_company.setText(_translate("CompanyMenuWidget", "New Company"))
        self.button_existing_company.setText(_translate("CompanyMenuWidget", "Existing Company"))

