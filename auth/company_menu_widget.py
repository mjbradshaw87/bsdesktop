from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget
from ui_company_menu_widget import Ui_CompanyMenuWidget


class CompanyMenuWidget(QWidget):
    """
    This is the company menu widget, shown if there is a new user.
    """

    new_company_button_clicked = pyqtSignal()
    existing_company_button_clicked = pyqtSignal()

    def __init__(self):
        """
        This is constructor for the CompanyMenuWidget.
        :return: Class Object for the CompanyMenuWidget.
        """
        super(CompanyMenuWidget, self).__init__()

        # set up the form
        self.ui = Ui_CompanyMenuWidget()
        self.ui.setupUi(self)

        # slots
        self.ui.button_new_company.clicked.connect(self.button_new_company_clicked)
        self.ui.button_existing_company.clicked.connect(self.button_existing_company_clicked)

    def button_new_company_clicked(self):
        """
        This is the method for the new company button.  When the button is clicked, it will
        broadcast a signal for the auth widget.  It will open a new company form.
        :return: Nothing
        """
        self.new_company_button_clicked.emit()

    def button_existing_company_clicked(self):
        """
        This is the method for the existing company button.  When the button is clicked, it will
        broadcast a signal for the auth widget.  It will open the employee code form to add a user
        without making a new company
        :return: Nothing
        """
        self.existing_company_button_clicked.emit()
