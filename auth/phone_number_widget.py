from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtWidgets import QWidget
from auth import const
from custom_controls.qprogressindicator import QProgressIndicator
from .ui_phone_number_widget import Ui_PhoneNumberWidget
from data.data_engine import DataEngine
import re


class PhoneNumberWidget(QWidget):
    """
    This is the controller for the phone widget form.
    This class will take in the users phone number, and it will send a request to the API
    and will get a response which contains a verification ID.

    Sends the number to the server and gets a verification ID or Error if something goes wrong.
    """
    # create signals
    got_error = pyqtSignal(['QString'], name='got_error')
    verification_id_received = pyqtSignal(['QString'], name='verification_id_received')

    def __init__(self, parent=None):
        """
        Constructor for the PhoneNumberWidget.
        :return: Class object of the PhoneNumberWidget
        """
        super(PhoneNumberWidget, self).__init__(parent)

        # make form
        self.ui = Ui_PhoneNumberWidget()
        self.ui.setupUi(self)

        # variables
        self.__non_decimal = re.compile(r'[^\d.]+')
        self.data_engine = DataEngine()
        self.phone = None
        self.progress = QProgressIndicator(self)
        self.ui.layout_activity.insertWidget(1, self.progress)

        # set up connections
        self.ui.line_phone_number.textChanged.connect(self.line_phone_number_value_changed)

    def showEvent(self, event):
        """
        This method forces focus on the phone number field
        :param event:
        :return:
        """
        if not self.ui.line_phone_number.hasFocus():
            # set phone number field in focus
            self.ui.line_phone_number.selectAll()
            self.ui.line_phone_number.setFocus(Qt.OtherFocusReason)

    def send_code(self, phone_number):
        """
        This method will send a formed number from the country code and the number line to the server.
        This will append the country code to the phone number given, then it will send the formed number to the
        server to get a verification code.
        :return:
        """
        # switch to load mode
        self.switch_load_mode()

        # make the number
        formed_number = str(self.ui.combo_country_codes.currentText()) + phone_number
        self.phone = formed_number

        # data_engine connections
        self.data_engine.response_received.connect(self.engine_response_received)
        self.data_engine.got_error.connect(self.request_error_received)

        # send a request
        self.data_engine.send_auth_request(
            url=const.GET_PIN_CODE_URL,
            params={'phone': formed_number}
        )

    def engine_response_received(self, response):
        """
        The method that gets the response from the engine.
        :param response: a dictionary containing verification id
        :return: void
        """
        # close connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # emit the verification id received
        self.verification_id_received.emit(response['verification_id'])

        # re enable form
        self.switch_load_mode(False)

    def request_error_received(self, error):
        """
        This method receives the error response from the engine.
        :param error: error string description
        :return: void
        """
        # close connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # clear the phone variable
        self.phone = None

        # report error
        self.got_error.emit(error)

        # re enable form
        self.switch_load_mode(False)

    def line_phone_number_value_changed(self, new_value):
        """
        This method will check the length of the line_phone_number param. If the length of the
        is greater then 1, the button will be enabled.
        :return: void
        """
        self.ui.line_phone_number.setText(self.__format_number(new_value))

        # send the code if we have a full number
        if len(self.ui.line_phone_number.text()) == 14:
            self.send_code(self.__non_decimal.sub('', str(self.ui.line_phone_number.text())))

    def __format_number(self, number):
        """
        This private method formats the number based on its length
        :param number: a number to format
        :return: the formatted number
        """
        # first, remove all non-numeric symbols
        number = self.__non_decimal.sub('', str(number))

        # parethesis check
        if len(number) >= 4:
            number = '(' + number[:3] + ') ' + number[3:]

        # dash check
        if len(number) >= 10:
            number = number[:9] + '-' + number[9:]

        return number

    def switch_load_mode(self, load_mode=True):
        """
        This method controls the state of the form (loading/static).
        :param load_mode: switch state control
        :return: void
        """
        self.progress.startAnimation() if load_mode else self.progress.stopAnimation()
        self.ui.line_phone_number.setEnabled(not load_mode)
        self.ui.combo_country_codes.setEnabled(not load_mode)
