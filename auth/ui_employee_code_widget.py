# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_employee_code_widget.ui'
#
# Created by: PyQt5 UI code generator 5.5
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_EmployeeCodeWidget(object):
    def setupUi(self, EmployeeCodeWidget):
        EmployeeCodeWidget.setObjectName("EmployeeCodeWidget")
        EmployeeCodeWidget.resize(496, 220)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(EmployeeCodeWidget)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(EmployeeCodeWidget)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.line_employee_code = QtWidgets.QLineEdit(EmployeeCodeWidget)
        self.line_employee_code.setMaxLength(10)
        self.line_employee_code.setAlignment(QtCore.Qt.AlignCenter)
        self.line_employee_code.setObjectName("line_employee_code")
        self.horizontalLayout_2.addWidget(self.line_employee_code)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        spacerItem4 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem4)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.button_back = QtWidgets.QPushButton(EmployeeCodeWidget)
        self.button_back.setObjectName("button_back")
        self.horizontalLayout.addWidget(self.button_back)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem5)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.horizontalLayout_3.addLayout(self.verticalLayout_2)
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem6)

        self.retranslateUi(EmployeeCodeWidget)
        QtCore.QMetaObject.connectSlotsByName(EmployeeCodeWidget)

    def retranslateUi(self, EmployeeCodeWidget):
        _translate = QtCore.QCoreApplication.translate
        EmployeeCodeWidget.setWindowTitle(_translate("EmployeeCodeWidget", "Form"))
        self.label.setText(_translate("EmployeeCodeWidget", "Please enter your employee registration code:"))
        self.line_employee_code.setPlaceholderText(_translate("EmployeeCodeWidget", "10-Digit Code"))
        self.button_back.setText(_translate("EmployeeCodeWidget", "Back"))

