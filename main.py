import sys
import os
from PyQt5.QtWidgets import QApplication
from widgets.main_window import MainWindow
from data.data_engine import find_data_file


# build package SSL trouble workaround
os.environ['REQUESTS_CA_BUNDLE'] = find_data_file('cacert.pem')


# create the application instance
app = QApplication(sys.argv)

# create main window
main = MainWindow()
main.start_app()

# run the application
# VS sucks
sys.exit(app.exec_())
