import datetime
import time
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QMessageBox, QDialogButtonBox
from custom_controls.qprogressindicator import QProgressIndicator
from data import const
from forms.ui_lauter_dialog import Ui_LauterDialog

__author__ = 'michaelbradshaw'


class LauterDialog(QDialog):
    """

    """
    # signal to report successful server interaction
    lauter_info_updated = pyqtSignal(['PyQt_PyObject'], name='lauter_info_updated')

    def __init__(self, data_engine, brew, parent=None):
        """

        :param data_engine: the data enigne to talk to the server
        :param brew: the users brews
        :param parent: the brew tracking widget
        :return: void
        """
        super(LauterDialog, self).__init__(parent)

        # setup UI
        self.ui = Ui_LauterDialog()
        self.ui.setupUi(self)
        self.brew = brew

        # setup Objects
        self.is_edit = len(self.brew['lauter_info']) > 0
        self.data_engine = data_engine
        self.progress = QProgressIndicator(self)
        self.ui.layout_status.insertWidget(0, self.progress)
        self.ui.label_status.setVisible(False)

        # behavior
        self.ui.label_status.setText(
            'Saving' if not self.is_edit else 'Updating' +
            ' the lauter info...'
        )
        self.setWindowTitle('New' if not self.is_edit else 'Edit' + ' Mash Info')

        # set maximum dates for all date/times
        self.ui.lauter_circ_start.setMaximumDate(datetime.datetime.now())
        self.ui.lauter_circ_end.setMaximumDate(datetime.datetime.now())
        self.ui.lauter_sparge_start.setMaximumDate(datetime.datetime.now())
        self.ui.lauter_sparge_end.setMaximumDate(datetime.datetime.now())
        self.ui.lauter_end_time.setMaximumDate(datetime.datetime.now())

        # setup the dialog if there is info to fill
        if not self.is_edit:
            self.ui.lauter_circ_start.setDateTime(datetime.datetime.now())
            self.ui.lauter_circ_end.setDateTime(datetime.datetime.now())
            self.ui.lauter_sparge_start.setDateTime(datetime.datetime.now())
            self.ui.lauter_sparge_end.setDateTime(datetime.datetime.now())
            self.ui.lauter_end_time.setDateTime(datetime.datetime.now())

        # set up the edit fields
        if self.is_edit:
            # convert all dates
            # print(int(self.brew['lauter_info']['circulation_start']))
            date_circ_start = datetime.datetime.fromtimestamp(int(self.brew['lauter_info']['circulation_start']))
            date_circ_end = datetime.datetime.fromtimestamp(int(self.brew['lauter_info']['circulation_end']))
            date_sparge_start = datetime.datetime.fromtimestamp(int(self.brew['lauter_info']['sparge_start']))
            date_sparge_end = datetime.datetime.fromtimestamp(int(self.brew['lauter_info']['sparge_end']))
            date_end_time = datetime.datetime.fromtimestamp(int(self.brew['lauter_info']['end_time']))

            # set all fields from the lauter_info
            self.ui.lauter_circ_start.setDateTime(date_circ_start)
            self.ui.lauter_circ_end.setDateTime(date_circ_end)
            self.ui.lauter_sparge_start.setDateTime(date_sparge_start)
            self.ui.lauter_sparge_end.setDateTime(date_sparge_end)
            self.ui.lauter_last_run.setValue(self.brew['lauter_info']['last_run'])
            self.ui.lauter_end_time.setDateTime(date_end_time)
            self.ui.lauter_first_wort.setValue(float(self.brew['lauter_info']['first_wort']))
            self.ui.lauter_total_time.setValue(float(self.brew['lauter_info']['total_time']))

        # setup signal
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)

#########################################################
#                         slots                         #
#########################################################

    def ok_button_clicked(self):
        """
        This is the slot for the ok button on the button_box.  If clicked,
        this method will call the connect the data engine, gather the params
        and set up the call to the server.
        :return: void
        """
        # display actions and lock form
        self.lock_the_form()

        # data_engine connections
        self.data_engine.response_received.connect(self.lauter_edit_saved)
        self.data_engine.got_error.connect(self.server_error_received)

        # set up params
        # start time is being converted to UTC before sending
        circ_start_utc = time.mktime(self.ui.lauter_circ_start.dateTime().toPyDateTime().timetuple())
        circ_end_utc = time.mktime(self.ui.lauter_circ_end.dateTime().toPyDateTime().timetuple())
        sparge_start_utc = time.mktime(self.ui.lauter_sparge_start.dateTime().toPyDateTime().timetuple())
        sparge_end_utc = time.mktime(self.ui.lauter_sparge_end.dateTime().toPyDateTime().timetuple())
        end_time_utc = time.mktime(self.ui.lauter_end_time.dateTime().toPyDateTime().timetuple())

        # # setup params
        params = {
            'circulation_start': circ_start_utc,
            'circulation_end': circ_end_utc,
            'sparge_start': sparge_start_utc,
            'sparge_end': sparge_end_utc,
            'last_run': float(self.ui.lauter_last_run.value()),
            'end_time': end_time_utc,
            'first_wort': float(self.ui.lauter_first_wort.value()),
            'total_time': float(self.ui.lauter_total_time.value())
        }

        # if edit mode
        if self.is_edit:
            params['lauter_info_id'] = self.brew['lauter_info']['id']
        else:
            params['brew_id'] = self.brew['id']

        # # send to the server
        self.data_engine.send_request(
            url=const.EDIT_LAUTER_INFO_URL if self.is_edit else const.ADD_LAUTER_INFO_URL,
            params=params,
            request_type='POST'
        )

#########################################################
#             data engine slots                         #
#########################################################

    def lauter_edit_saved(self, response):
        """
        This is the slot for the server response.  If there is no error, then the server
        will call this slot and will close the form.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # unlock form
        self.lock_the_form(False)

        # report success
        self.lauter_info_updated.emit(response['lauter_info'])

        # exit dialog
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_the_form(False)

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def lock_the_form(self, lock=True):
        """
        This method will lock the form so the user can use anything while its busy.
        :return: void
        """
        self.ui.group_lauter.setEnabled(not lock)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)
        self.ui.label_status.setHidden(not lock)
        self.progress.startAnimation() if lock else self.progress.stopAnimation()
