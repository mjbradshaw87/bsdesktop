from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QMessageBox
from custom_controls.qprogressindicator import QProgressIndicator
from data import const
from forms.ui_brew_dialog import Ui_BrewDialog


class BrewDialog(QDialog):
    """
    This is the dialog when the user makes a new active brew.  This class will commit a new
    active brew to the server
    """
    # signal to report successful server interaction
    brew_info_updated = pyqtSignal(['PyQt_PyObject'], name='brew_info_updated')

    # noinspection PyUnresolvedReferences
    def __init__(self, data_engine, recipe=None, brew=None, parent=None):
        """
        This is the constuctor for the NewBrewDialog
        :param data_engine: The apps data engine to communicate with the server
        :param recipe: The recipe made by the user stored in the recipes table, this recipe stores all
        the ingredients used for the brew process, we will be pulling items form there to
        fill into the brew sheet
        :param parent: the brew trackign widget
        :return: void
        """
        super(BrewDialog, self).__init__(parent)

        # setup ui
        self.ui = Ui_BrewDialog()
        self.ui.setupUi(self)

        # setup data
        self.data_engine = data_engine
        self.recipe = recipe
        self.brew = brew

        # set up the form if editiing
        if self.brew:
            self.ui.line_recipe_name.setText(str(brew['recipe_id']))
            self.ui.line_batch_number.setText(brew['batch_number'])
            self.ui.line_brew_number.setText(brew['brew_code'])
            self.ui.line_brewer.setText(brew['brewer_name'])
            self.ui.line_fv_number.setText(brew['fv_number'])

        if self.recipe:
            self.recipe = recipe
            self.ui.line_recipe_name.setText(recipe['name'])

        # create indicator
        self.progress = QProgressIndicator(self)
        self.ui.layout_status.insertWidget(0, self.progress)

        # behaviors
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(False)
        self.setWindowTitle(
            'Add A New ' if self.recipe else 'Edit An' + ' Active Brew'
        )
        self.ui.label_status.setText(
            'Adding an' if self.recipe else 'Editing the' + ' active brew...'
        )
        self.ui.label_status.setVisible(False)

        # setup
        self.line_fields = [
            self.ui.line_recipe_name,
            self.ui.line_batch_number,
            self.ui.line_brew_number,
            self.ui.line_fv_number,
            self.ui.line_brewer
        ]

        # setup connections
        for element in self.line_fields:
            element.textChanged.connect(self.validate_form)
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)

#########################################################
#                       Slots                           #
#########################################################

    def validate_form(self):
        """
        This method will check to see if all fields have been filled out, and if they have, it will enable the
        button box "ok" button.
        :return: void
        """
        lengths = map(lambda x: len(x.text()), self.line_fields)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(all(lengths))

    def ok_button_clicked(self):
        """
        This is the slot for the button "ok" being clicked by the user.  This button will only be
        enabled after the form has been validated.  This method will connect the data_engine, and send a request
        to the server to add a recipe.
        :return: void
        """
        self.progress.startAnimation()

        # lock the form so the user cant break it
        self.lock_the_form(True)

        # Send the new recipe to the server
        # data_engine connections
        self.data_engine.response_received.connect(self.brew_tracking_started)
        self.data_engine.got_error.connect(self.server_error_received)

        # prepare other params
        params = {
            'brew_code': str(self.ui.line_brew_number.text()),
            'batch_number': str(self.ui.line_batch_number.text()),
            'fv_number': str(self.ui.line_fv_number.text()),
            'brewer_name': str(self.ui.line_brewer.text())
        }

        if self.recipe:
            params['recipe_id'] = str(self.recipe['id'])

        if self.brew:
            params['brew_id'] = str(self.brew['id'])

        # send a request to the server
        self.data_engine.send_request(
            url=const.ADD_BREW_URL if self.recipe else const.EDIT_BREW_URL,
            params=params,
            request_type='POST'
        )

#########################################################
#             data engine slots                         #
#########################################################

    def brew_tracking_started(self, response):
        """
        This is the slot for a successful call to the data engine.  If there is no error, the method
        will disconnect from the data engine, and enable the form, from there it will bring the user
        back to the brew tracking widget.
        :return: void
        """
        # disconnect signals from engine
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # revert business
        self.progress.stopAnimation()

        # unlock the form
        self.lock_the_form()

        # emit update signal
        self.brew_info_updated.emit(response['brew'])

        # proceed with ok button
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_the_form()

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def lock_the_form(self, lock=True):
        """
        This method will lock the form so the user cant use anything while its busy.
        :return: void
        """
        self.ui.group_brew.setEnabled(not lock)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)
        self.ui.label_status.setVisible(lock)
        self.progress.startAnimation() if lock else self.progress.stopAnimation()

