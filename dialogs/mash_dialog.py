import datetime
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QMessageBox, QDialogButtonBox
from forms.ui_mash_dialog import Ui_MashDialog
from custom_controls.qprogressindicator import QProgressIndicator
import time
import data.const as const


class MashDialog(QDialog):
    """
    This is the mash dialog class. It enables the user to edit dialog fields
    for their brew sheet.
    """
    # signal to report successful server interaction
    mash_info_updated = pyqtSignal(['PyQt_PyObject'], name='mash_info_updated')

    # noinspection PyUnresolvedReferences
    def __init__(self, data_engine, brew, parent=None):
        """

        :param data_engine: the data enigne to talk to the server
        :param brew: the users active brews
        :param parent: brew tracking widget
        :return: void
        """
        super(MashDialog, self).__init__(parent)

        # setup ui
        self.ui = Ui_MashDialog()
        self.ui.setupUi(self)
        self.brew = brew

        # setup Objects
        self.is_edit = len(self.brew['mash_info']) > 0
        self.data_engine = data_engine
        self.progress = QProgressIndicator(self)
        self.ui.layout_status.insertWidget(0, self.progress)
        self.ui.label_status.setVisible(False)

        # behavior
        self.ui.label_status.setText(
            'Saving' if not self.is_edit else 'Updating' +
            ' the mash info...'
        )

        self.setWindowTitle('New' if not self.is_edit else 'Edit' + ' Mash Info')

        # set max dates for the date objects
        self.ui.mash_start_time.setMaximumDate(datetime.datetime.now())
        self.ui.mash_sac_test.setMaximumDate(datetime.datetime.now())

        # set up the dialog if there is information to fill
        if self.is_edit:
            date_sacc = datetime.datetime.fromtimestamp(int(self.brew['mash_info']['saccaride_test_time']))
            self.ui.mash_sac_test.setDateTime(date_sacc)
            date_start = datetime.datetime.fromtimestamp(int(self.brew['mash_info']['start_time']))
            self.ui.mash_start_time.setDateTime(date_start)
            self.ui.mash_ph.setValue(float(self.brew['mash_info']['ph']))
            self.ui.mash_water_bbl.setValue(float(self.brew['mash_info']['mash_water']))
            self.ui.mash_found_bbl.setValue(float(self.brew['mash_info']['foundation']))
        else:
            # set current date/time for the date slots
            self.ui.mash_sac_test.setDateTime(datetime.datetime.now())
            self.ui.mash_start_time.setDateTime(datetime.datetime.now())

        # setup signal
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)

#########################################################
#                         slots                         #
#########################################################

    def ok_button_clicked(self):
        """
        This is the slot for the ok button on the button_box.  If clicked,
        this method will call the connect the data engine, gather the params
        and set up the call to the server.
        :return: void
        """
        # display actions and lock form
        self.lock_the_form()

        # data_engine connections
        self.data_engine.response_received.connect(self.mash_edit_saved)
        self.data_engine.got_error.connect(self.server_error_received)

        # set up params
        # start time is being converted to UTC before sending
        start_utc = time.mktime(self.ui.mash_start_time.dateTime().toPyDateTime().timetuple())

        # setup params
        params = {
            'start_time': start_utc,
            'foundation': int(self.ui.mash_found_bbl.value()),
            'mash_water': int(self.ui.mash_water_bbl.value()),
            'ph': int(self.ui.mash_ph.value()),
            'saccaride_test_time': time.mktime(self.ui.mash_sac_test.dateTime().toPyDateTime().timetuple())
        }

        # if edit mode
        if self.is_edit:
            params['mash_info_id'] = self.brew['mash_info']['id']
        else:
            params['brew_id'] = self.brew['id']

        # send to the server
        self.data_engine.send_request(
            url=const.EDIT_MASH_INFO_URL if self.is_edit else const.ADD_MASH_INFO_URL,
            params=params,
            request_type='POST'
        )

#########################################################
#             data engine slots                         #
#########################################################

    def mash_edit_saved(self, response):
        """
        This is the slot for the server response.  If there is no error, then the server
        will call this slot and will close the form.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # unlock form
        self.lock_the_form(False)

        # report success
        self.mash_info_updated.emit(response['mash_info'])

        # exit dialog
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_the_form(False)

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def lock_the_form(self, lock=True):
        """
        This method will lock the form so the user cant use anything while its busy.
        :return: void
        """
        self.ui.group_mash.setEnabled(not lock)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)
        self.ui.label_status.setVisible(not lock)
        self.progress.startAnimation() if lock else self.progress.stopAnimation()
