from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QMessageBox
from data import const
from forms.ui_wort_dialog import Ui_WortDialog
from custom_controls.qprogressindicator import QProgressIndicator

__author__ = 'michaelbradshaw'


class WortDialog(QDialog):
    """

    """
    # signal to report successful server interaction
    wort_info_updated = pyqtSignal(['PyQt_PyObject'], name='wort_info_updated')

    # noinspection PyUnresolvedReferences
    def __init__(self, data_engine, brew, parent=None):
        """

        :param date_engine:
        :param brew: the users active brews
        :param parent: brew tracking widget
        :return: void
        """
        super(WortDialog, self).__init__(parent)

        # setup ui
        self.ui = Ui_WortDialog()
        self.ui.setupUi(self)

        # setup objects
        self.brew = brew
        self.data_engine = data_engine
        self.is_edit = len(self.brew['wort_info']) > 0
        self.progress = QProgressIndicator(self)
        self.ui.layout_status.insertWidget(0, self.progress)
        self.ui.label_status.setVisible(False)

        # setup behaviors
        self.setWindowTitle(
            'Add' if not self.is_edit else 'Edit' + ' Wort Info'
        )
        self.ui.label_status.setText(
            'Saving' if not self.is_edit else 'Editing' + ' wort info...'
        )

        # setup data if is_edit
        if self.is_edit:
            print(brew)
            self.ui.double_wort_volume.setValue(self.brew['wort_info']['wort_volume_bbl'])
            self.ui.double_og.setValue(self.brew['wort_info']['og'])
            self.ui.double_color_srm.setValue(self.brew['wort_info']['color_srm'])

        # setup signal
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)
        # self.ui.double_color_srm.connect(self.changed_params(''))

#########################################################
#                         slots                         #
#########################################################

    def changed_params(self, changed_param):
        pass

    def ok_button_clicked(self):
        """
        This is the slot for the ok button on the button_box.  If clicked,
        this method will call the connect the data engine, gather the params
        and set up the call to the server.
        :return: void
        """
        # display actions and lock form
        self.lock_the_form()

        # data_engine connections
        self.data_engine.response_received.connect(self.wort_edit_saved)
        self.data_engine.got_error.connect(self.server_error_received)

        params = {
            'wort_volume_bbl': self.ui.double_wort_volume.value(),
            'og': self.ui.double_og.value(),
            'color_srm': self.ui.double_color_srm.value()
        }

        # if edit mode
        if self.is_edit:
            params['wort_info_id'] = self.brew['wort_info']['id']
        else:
            params['brew_id'] = self.brew['id']

        print(params)

        # send to the server
        self.data_engine.send_request(
            url=const.EDIT_WORT_INFO_URL if self.is_edit else const.ADD_WORT_INFO_URL,
            params=params,
            request_type='POST'
        )

#########################################################
#             data engine slots                         #
#########################################################

    def wort_edit_saved(self, response):
        """
        This is the slot for the server response.  If there is no error, then the server
        will call this slot and will close the form.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # unlock form
        self.lock_the_form(False)

        # report success
        self.wort_info_updated.emit(response['wort_info'])

        # exit dialog
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_the_form(False)

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def lock_the_form(self, lock=True):
        """
        This method will lock the form so the user cant use anything while its busy.
        :return: void
        """
        self.ui.group_wort.setEnabled(not lock)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)
        self.ui.label_status.setVisible(not lock)
        self.progress.startAnimation() if lock else self.progress.stopAnimation()

