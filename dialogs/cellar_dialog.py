from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QMessageBox, QDialogButtonBox
from custom_controls.qprogressindicator import QProgressIndicator
from data import const
from forms.ui_cellar_dialog import Ui_CellarDialog
import datetime
import time

__author__ = 'michaelbradshaw'


class CellarDialog(QDialog):
    """

    """
    # signal to report successful server interaction
    cellar_info_updated = pyqtSignal(['PyQt_PyObject'], name='cellar_info_updated')

    def __init__(self, data_engine, brew, parent=None):
        """

        :param date_engine:
        :param brew: the users active brews
        :param parent: brew tracking widget
        :return: void
        """
        super(CellarDialog, self).__init__(parent)

        # setup ui
        self.ui = Ui_CellarDialog()
        self.ui.setupUi(self)

        # setup Objects
        self.brew = brew
        self.is_edit = len(self.brew['cellar_info']) > 0
        self.data_engine = data_engine
        self.progress = QProgressIndicator(self)
        self.ui.layout_status.insertWidget(0, self.progress)
        self.ui.label_status.setVisible(False)

        # setup behaviors
        self.setWindowTitle(
            'New' if not self.is_edit else "Edit" + ' Cellar Info'
        )
        self.ui.label_status.setText(
            'Saving' if not self.is_edit else 'Editing' + ' cellar info...'
        )

        # set max dates for the date object
        self.ui.cellar_blow_off.setMaximumDateTime(datetime.datetime.now())
        self.ui.cellar_blow_off.setDateTime(datetime.datetime.now())

        # add info if is_edit
        if self.is_edit:
            date_blow_off = datetime.datetime.fromtimestamp(int(self.brew['cellar_info']['blow_off_attach']))
            self.ui.cellar_blow_off.setDateTime(date_blow_off)
            self.ui.double_yeast.setValue(self.brew['cellar_info']['yeast_type'])
            self.ui.check_present.setChecked(self.brew['cellar_info']['glycol'])

        # setup signal
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)


#########################################################
#                         slots                         #
#########################################################

    def ok_button_clicked(self):
        """
        This is the slot for the ok button on the button_box.  If clicked,
        this method will call the connect the data engine, gather the params
        and set up the call to the server.
        :return: void
        """
        # display actions and lock form
        self.lock_the_form()

        # data_engine connections
        self.data_engine.response_received.connect(self.cellar_edit_saved)
        self.data_engine.got_error.connect(self.server_error_received)

        # set up params
        # start time is being converted to UTC before sending
        blow_off_utc = time.mktime(self.ui.cellar_blow_off.dateTime().toPyDateTime().timetuple())

        # setup params
        params = {
            'glycol': self.ui.check_present.isChecked(),
            'blow_off_attach': blow_off_utc,
            'yeast_type': self.ui.double_yeast.value()
        }

        # if edit mode
        if self.is_edit:
            params['cellar_info_id'] = self.brew['cellar_info']['id']
        else:
            params['brew_id'] = self.brew['id']

        # send to the server
        self.data_engine.send_request(
            url=const.EDIT_CELLAR_INFO_URL if self.is_edit else const.ADD_CELLAR_INFO_URL,
            params=params,
            request_type='POST'
        )


#########################################################
#             data engine slots                         #
#########################################################

    def cellar_edit_saved(self, response):
        """
        This is the slot for the server response.  If there is no error, then the server
        will call this slot and will close the form.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # unlock form
        self.lock_the_form(False)

        # report success
        self.cellar_info_updated.emit(response['cellar_info'])

        # exit dialog
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_the_form(False)

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def lock_the_form(self, lock=True):
        """
        This method will lock the form so the user cant use anything while its busy.
        :return: void
        """
        self.ui.group_cellar.setEnabled(not lock)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)
        self.ui.label_status.setVisible(not lock)
        self.progress.startAnimation() if lock else self.progress.stopAnimation()
