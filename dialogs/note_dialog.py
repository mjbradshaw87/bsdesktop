from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QMessageBox
from data import const
from forms.ui_notes_dialog import Ui_NotesDialog
from custom_controls.qprogressindicator import QProgressIndicator

__author__ = 'michaelbradshaw'


# noinspection PyUnresolvedReferences
class NotesDialog(QDialog):
    """

    """
    # signal to report successful server interaction
    note_updated = pyqtSignal(['PyQt_PyObject', bool], name='note_updated')

    def __init__(self, data_engine, brew_id, note=None, parent=None):
        """

        :param data_engine:
        :return: void
        """
        super(NotesDialog, self).__init__(parent)

        # setup ui
        self.ui = Ui_NotesDialog()
        self.ui.setupUi(self)

        # setup objects
        self.note = note
        self.brew_id = brew_id
        self.data_engine = data_engine
        self.progress = QProgressIndicator(self)

        # set up layout
        self.ui.layout_status.insertWidget(0, self.progress)
        self.ui.label_status.setVisible(False)
        self.ui.label_status.setText(
            ('Saving new' if not self.note else 'Editing') + ' note...'
        )
        self.setWindowTitle(
            ('Edit A' if self.note else 'Write A New') + ' Note'
        )

        # intial disable of ok button till form validation
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(False)

        # set up the dialog with info if this is an edit
        if self.note:
            self.ui.line_note_title.setText(self.note['title'])
            self.ui.text_note.setPlainText(self.note['content'])

        self.ui.label_count.setText(
            'Characters Left: ' + str(500 - len(self.ui.text_note.toPlainText()))
        )

        # setup signals connections
        self.ui.line_note_title.textChanged.connect(self.validate_form)
        self.ui.text_note.textChanged.connect(self.validate_form)
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)

#########################################################
#                       Slots                           #
#########################################################

    def validate_form(self):
        """

        :return: void
        """
        # validate note content
        count = len(self.ui.text_note.toPlainText())
        self.ui.label_count.setText('Characters Left: ' + str(500 - count))
        limit_exceeded = count > 500
        self.ui.label_count.setStyleSheet('QLabel { ' + ('color: red;}' if limit_exceeded else '}'))

        # control the ok button
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(
            not limit_exceeded and count > 0 and len(self.ui.line_note_title.text())
        )

    def ok_button_clicked(self):
        """

        :return: void
        """
        # disable the form
        self.enable_the_form(enable=False)

        # data engine connections
        self.data_engine.response_received.connect(self.note_saved)
        self.data_engine.got_error.connect(self.server_error_received)

        # setup params
        title = self.ui.line_note_title.text()
        content = self.ui.text_note.toPlainText()

        params = {
            'title': title,
            'content': content,
        }

        if self.note:
            params['note_id'] = self.note['id']
        else:
            params['brew_id'] = self.brew_id

        self.data_engine.send_request(
            url=const.EDIT_NOTE_URL if self.note else const.ADD_NOTE_URL,
            params=params,
            request_type='POST'
        )


#########################################################
#             data engine slots                         #
#########################################################

    def note_saved(self, response):
        """

        :return: void
        """
        # disconnect signals from engine
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # revert business
        self.enable_the_form()

        # report success
        self.note_updated.emit(response['note'], not self.note)

        # proceed with ok button
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.enable_the_form()

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def enable_the_form(self, enable=True):
        """
        This method will determine if the form should be enabled or not.  It is passed a param
        to help the method enable or disable the form
        :param enable: bool, indicates if you want the form locked or not
        :return: void
        """
        self.ui.group_note.setEnabled(enable)
        self.progress.startAnimation() if not enable else self.progress.stopAnimation()
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(enable)
        self.ui.label_status.setVisible(not enable)
