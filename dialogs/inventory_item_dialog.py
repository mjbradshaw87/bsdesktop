from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator
from data import const
from forms.ui_inventory_item_dialog import Ui_InventoryItemDialog
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QMessageBox
from custom_controls.qprogressindicator import QProgressIndicator


class InventoryItemDialog(QDialog):
    """
    The add inventory dialog class.  This dialog will add an inventory item to the server
    through the data engine.  It will prompt users about the new item to help the user
    add the item into their database.
    """
    # noinspection PyUnresolvedReferences
    def __init__(self, data_engine, item=None, units=None, classes=None, parent=None):
        super(InventoryItemDialog, self).__init__(parent)

        # setup UI
        self.ui = Ui_InventoryItemDialog()
        self.ui.setupUi(self)

        # setup objects
        self.item = item
        self.data_engine = data_engine
        self.progress = QProgressIndicator(self)
        self.ui.layout_status.insertWidget(0, self.progress)
        self.ui.combo_units.addItems(units if units else [])
        self.ui.combo_units.setEditable(True)
        self.ui.combo_class.addItems(classes if classes else [])
        self.ui.combo_class.setEditable(True)

        validator = QRegExpValidator(QRegExp('[A-Za-z_]+'), self)
        self.ui.combo_units.lineEdit().setValidator(validator)
        self.ui.combo_class.lineEdit().setValidator(validator)

        # setup behaviors
        self.ui.label_status.setVisible(False)
        self.ui.label_status.setText('Adding' if not self.item else 'Editing' + ' inventory item...')

        # setup connections
        self.ui.line_name.textChanged.connect(self.validate_form)
        self.ui.line_supplier.textChanged.connect(self.validate_form)
        self.ui.combo_class.activated.connect(self.validate_form)
        self.ui.combo_units.activated.connect(self.validate_form)
        self.ui.combo_class.editTextChanged.connect(self.validate_form)
        self.ui.combo_units.editTextChanged.connect(self.validate_form)
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)
        self.ui.double_current.valueChanged.connect(self.validate_form)
        self.ui.double_price.valueChanged.connect(self.validate_form)
        self.ui.double_rerder.valueChanged.connect(self.validate_form)

        if self.item:
            # put all the existing item data into fields
            self.ui.line_name.setText(item.name)
            self.ui.line_supplier.setText(item.supplier)
            self.ui.combo_units.setCurrentIndex(units.index(item.units))
            self.ui.combo_class.setCurrentIndex(classes.index(item.item_class))
            self.ui.line_extra.setText(item.extra)
            self.ui.double_current.setValue(item.amount)
            self.ui.double_rerder.setValue(item.reorder_amount)
            self.ui.double_price.setValue(item.price)

            self.setWindowTitle('Edit Inventory Item')
            self.ui.button_box.button(QDialogButtonBox.Ok).setText('Edit')

        # ok/edit button has to be disabled by default
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(False)

    def validate_form(self):
        """

        :return:
        """
        has_line_info = len(self.ui.line_name.text()) and len(self.ui.line_supplier.text())
        has_combo_info = len(self.ui.combo_class.currentText()) and len(self.ui.combo_units.currentText())

        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(has_line_info and has_combo_info)

    def ok_button_clicked(self):
        """
        This method will call the API and add an item.  This method will create a dictionary
        for the server with the fields input by the user in the data widget.  It then calls the
        server from the data engine.
        :return: Nothing.
        """
        # lock the form
        self.lock_form()

        # create item dictionary based on the fields
        item_dict = {
            'name': self.ui.line_name.text(),
            'item_class': self.ui.combo_class.currentText(),
            'supplier': self.ui.line_supplier.text(),
            'amount': float(self.ui.double_current.value()),
            'reorder_amount': float(self.ui.double_rerder.value()),
            'unit': self.ui.combo_units.currentText(),
            'price': float(self.ui.double_price.value())
        }

        # if the item exists and we are in edit mode grab the items ID
        if self.item:
            item_dict['id'] = self.item.id

        # check to see if the extra field has text
        if len(self.ui.line_extra.text()):
            item_dict['extra_field'] = self.ui.line_extra.text()

        # data_engine connections
        self.data_engine.response_received.connect(self.item_added_received)
        self.data_engine.got_error.connect(self.server_error_received)

        # send a request to the data engine
        self.data_engine.send_request(
            url=const.ADD_ITEM_INTO_DATABASE_URL if not self.item else const.EDIT_ITEM_INTO_DATABASE_URL,
            params=item_dict,
            request_type='POST'
        )

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: Nothing.
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_form(False)

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

    def item_added_received(self, response):
        """

        :return:
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # revert business
        self.ui.label_status.setText('Item has been ' + 'added' if self.item else 'updated')

        # unlock the form
        self.lock_form(False)

        # proceed with ok button
        self.accept()

    def lock_form(self, lock=True):
        """

        :param lock:
        :return:
        """
        self.progress.startAnimation() if lock else self.progress.stopAnimation()

        # lock the form so the user cant break it
        self.ui.label_status.setVisible(lock)
        self.ui.group_item.setEnabled(not lock)

        # lock the buttons
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)
