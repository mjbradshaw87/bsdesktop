from PyQt5.QtWidgets import QDialog, QMessageBox, QDialogButtonBox
from forms.ui_bbl_dialog import Ui_BblDialog
from custom_controls.qprogressindicator import QProgressIndicator


class BblDialog(QDialog):
    """
    Class doc
    """
    # noinspection PyUnresolvedReferences
    def __init__(self, parent=None, data_engine=None):
        """
        constr doc
        :param parent:
        :return: void
        """
        super(BblDialog, self).__init__(parent)

        # setup ui
        self.ui = Ui_BblDialog()
        self.ui.setupUi(self)

        # setup objects
        self.data_engine = data_engine
        self.progress = QProgressIndicator(self)
        self.ui.layout_status.insertWidget(0, self.progress)
        self.ui.label_status.setVisible(False)

        # connections
        self.ui.button_box.rejected.connect(self.reject)
        self.ui.button_box.accepted.connect(self.ok_button_clicked)

#########################################################
#                         slots                         #
#########################################################

    def ok_button_clicked(self):
        """
        This is the slot for the button box "ok" button clicked.
        :return: void
        """
        # data_engine connections
        self.data_engine.response_received.connect(self.bbl_edit_saved)
        self.data_engine.got_error.connect(self.server_error_received)

        # display actions and lock form
        self.lock_the_form()

        # set up params
        # params = {
        # start_amount: self.ui.double_start.value(),
        # end_amount: self.ui.double_end.value()
        # }

#########################################################
#             data engine slots                         #
#########################################################

    def bbl_edit_saved(self):
        """
        This is the slot for the server response.  If there is no error, then the server
        will call this slot and will close the form.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # unlock form
        self.lock_the_form(False)
        self.ui.label_status.setText(' ')

        # exit dialog
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_the_form(False)
        self.ui.label_status.setText(' ')

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def lock_the_form(self, lock=True):
        """
        This method will lock the form so the user can use anything while its busy.
        :return: void
        """
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)
        self.ui.group_info.setEnabled(not lock)
        self.ui.label_status.setText("Saving levels...")
        self.ui.label_status.setVisible(lock)
        self.progress.startAnimation() if lock else self.progress.stopAnimation()
