from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import (
    QDialog, QItemDelegate, QComboBox, QDoubleSpinBox,
    QTableWidgetItem, QDialogButtonBox, QMessageBox
)
from forms.ui_recipe_dialog import Ui_RecipeDialog
from custom_controls.qprogressindicator import QProgressIndicator
from data import const


class RecipeDialog(QDialog):
    """
    This is the Recipe dialog class.  It is a dialog for the user to create a recipe or edit
    an existing recipe.
    """
    # noinspection PyUnresolvedReferences
    def __init__(self, data_engine, items, edit_recipe=None, parent=None):
        """
        constructor for the RecipeDialog class.
        :param data_engine: The data engine to call the server
        :param items: items from the users inventory
        :param parent: The parent (brew tracking widget) of the dialog
        :return: void
        """
        super(RecipeDialog, self).__init__(parent)

        # setup ui
        self.ui = Ui_RecipeDialog()
        self.ui.setupUi(self)
        self.resize(350, 500)
        self.setMinimumWidth(350)

        # data and variables
        self.recipe = edit_recipe
        self.data_engine = data_engine
        self.items = items
        self.ingridients = []

        # if we're in the edit mode
        if self.recipe:
            self.ui.line_recipe_name.setText(self.recipe['name'])
            self.ingridients = self.make_ingridients_from_recipe(edit_recipe)

            self.setWindowTitle('Edit Recipe')
            self.ui.button_box.button(QDialogButtonBox.Ok).setText('Edit')

            # fill the table with data
            self.fill_table()

            # disable the add button if all the items are used
            if len(self.ingridients) == len(self.items):
                self.ui.button_add.setEnabled(False)

        # set up the number of rows
        self.ui.table_recipe.setRowCount(len(self.ingridients))

        # set delegates
        combo_delegate = ComboBoxDelegate(
            ingridients=self.ingridients,
            items=self.items,
            parent=self.ui.table_recipe
        )

        spin_delegate = SpinBoxDelegate(
            ingridients=self.ingridients,
            items=self.items,
            parent=self.ui.table_recipe
        )

        # create indicator
        self.progress = QProgressIndicator(self)
        self.ui.layout_indicator.insertWidget(0, self.progress)

        # behaviors
        self.ui.button_remove.setEnabled(False)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(False)

        self.ui.table_recipe.setItemDelegateForColumn(0, combo_delegate)
        self.ui.table_recipe.setItemDelegateForColumn(1, spin_delegate)

        # setup connections
        self.ui.line_recipe_name.textChanged.connect(self.validate_form)
        self.ui.table_recipe.cellChanged.connect(self.validate_form)
        self.ui.table_recipe.itemSelectionChanged.connect(self.remove_button_activate)
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)
        self.ui.button_add.clicked.connect(self.add_button_clicked)
        self.ui.button_remove.clicked.connect(self.remove_button_clicked)
        combo_delegate.item_selected.connect(self.item_selection_changed)
        spin_delegate.amount_entered.connect(self.item_amount_entered)

#########################################################
#                         slots                         #
#########################################################

    def recipe_info(self):
        """
        If there is a recipe to edit, this will put the ingredient information into the 
        :return: void
        """
        def ing_without_item_index(ing):
            try:
                del ing['item_index']
            except:
                pass
            return ing

        return {
            'name': str(self.ui.line_recipe_name.text()),
            'ingridients': map(ing_without_item_index, self.ingridients)
        }

    def ok_button_clicked(self):
        """
        This method saves the recipe to the server
        :return: void
        """
        self.ui.label_status.setText('Saving the recipe...')
        self.progress.startAnimation()

        self.lock_the_form()

        # Send the new recipe to the server
        # data_engine connections
        self.data_engine.response_received.connect(self.recipe_saved)
        self.data_engine.got_error.connect(self.server_error_received)

        # prepare ingridients
        ingridients_string = ';'.join(map(lambda x: str(x['item_id']) + '=' + str(x['amount']), self.ingridients))

        # prepare other params
        params = {
            'name': str(self.ui.line_recipe_name.text()),
            'ingridients': ingridients_string
        }

        if self.recipe:
            params['id'] = self.recipe['id']

        # send a request to the server
        self.data_engine.send_request(
            url=const.ADD_RECIPE_INTO_DATABASE_URL if not self.recipe else const.EDIT_RECIPE_INTO_DATABASE_URL,
            params=params,
            request_type='POST'
        )

    def add_button_clicked(self):
        """
        This is the slot for the add button being clicked.  It will add a row to the table,
        then it will scroll to the bottom of the list.  From there, it will be active and let the
        user insert the ingredient that they want and the amount that they want to use from
        and spinbox and a combobox.
        :return: void
        """
        # disable until we can add an item again (until data has been entered)
        self.ui.button_add.setEnabled(False)

        # add one more row to the table
        self.ui.table_recipe.insertRow(self.ui.table_recipe.rowCount())
        self.ui.table_recipe.scrollToBottom()

        # create and then edit the new item cell
        new_cell_item = QTableWidgetItem()
        self.ui.table_recipe.setItem(self.ui.table_recipe.rowCount() - 1, 0, new_cell_item)
        self.ui.table_recipe.setCurrentItem(new_cell_item)
        self.ui.table_recipe.editItem(new_cell_item)

    def remove_button_clicked(self):
        """
        This is slot for the remove recipe button when its clicked.  It will gather the
        row number and call the server to remove the recipe from the database.
        :return: Nothing
        """
        selected_rows = map(lambda item: item.row(), self.ui.table_recipe.selectedItems())
        selected_rows = list(set(selected_rows))

        for row in sorted(selected_rows, reverse=True):
            if len(self.ingridients) == self.ui.table_recipe.rowCount():
                # remove the ingridient
                del self.ingridients[row]
            else:
                # re-enable add button
                self.ui.button_add.setEnabled(True)

            # remove the row from the table
            self.ui.table_recipe.removeRow(row)

        # clear selection in the table
        self.ui.table_recipe.clearSelection()

        # disable remove button again
        self.ui.button_remove.setEnabled(False)

        # enable add button, if needed
        self.ui.button_add.setEnabled(len(self.ingridients) < len(self.items))

    def remove_button_activate(self):
        """
        This method activates the remove button on the dialog
        :return: void
        """
        self.ui.button_remove.setEnabled(len(self.ui.table_recipe.selectedItems()))

    def make_ingridients_from_recipe(self, recipe):
        """
        This method creates a list of ingridients out of a recipe
        :param recipe:
        :return ingridints:
        """
        ingridients = []
        item_indeces = list(map(lambda item: item.id, self.items))

        for ingridient in recipe['ingridients']:
            item_id = ingridient['item_id']
            item_index = item_indeces.index(item_id)

            ingridient = {
                'item_id': item_id,
                'item_index': item_index,
                'amount': ingridient['amount']
            }

            ingridients.append(ingridient)

        return ingridients

    def fill_table(self):
        """
        This method fills the table with existing data when the dialog is in the existing mode.
        :return: void
        """
        for i, ingridient in enumerate(self.ingridients):
            # get the corresponding item
            ing_item = self.items[ingridient['item_index']]

            # prepare the value string
            value = ingridient['amount']
            value_format = '{:.1f}' if round(value, 1) == round(value, 2) else '{:.2f}'

            # set up cells
            name_item = QTableWidgetItem(ing_item.name)
            value_item = QTableWidgetItem(value_format.format(value) + ' ' + ing_item.units)
            self.ui.table_recipe.setItem(i, 0, name_item)
            self.ui.table_recipe.setItem(i, 1, value_item)

    def item_selection_changed(self, row, item_index):
        """
        This is the slot
        :return: void
        """
        # save the newly selected item
        if row < len(self.ingridients):
            ingridient = self.ingridients[row]
            ingridient['item_index'] = item_index
            ingridient['item_id'] = self.items[item_index].id
        else:
            ingridient = {
                'item_id': self.items[item_index].id,
                'item_index': item_index,
                'amount': 1.0
            }

            # add the ingridient
            self.ingridients.append(ingridient)

        # set the focus on the spinbox
        cell_item = self.ui.table_recipe.item(row, 1)
        if not cell_item:
            cell_item = QTableWidgetItem()
            self.ui.table_recipe.setItem(row, 1, cell_item)

        self.ui.table_recipe.setCurrentItem(cell_item)
        self.ui.table_recipe.editItem(cell_item)

    def item_amount_entered(self, row, amount):
        """

        :param row:
        :param amount:
        :return: void
        """
        # save the amount
        ingridient = self.ingridients[row]
        ingridient['amount'] = amount

        # enable add button again (if there are any items available to add)
        if len(self.ingridients) != len(self.items):
            self.ui.button_add.setEnabled(True)

    def validate_form(self):
        """
        This method will determine if the from has enough information to enable the
        button box to make a recipe.
        :return: void
        """
        has_name = len(self.ui.line_recipe_name.text()) > 0
        has_ingridients = len(self.ingridients) > 0

        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(has_name and has_ingridients)

#########################################################
#             data engine slots                         #
#########################################################

    def recipe_saved(self):
        """
        This is the slot for a successful save from the data engine.  If there is no errors making
        a recipe, then this will get called.
        :return: void
        """
        # disconnect signals from engine
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # revert business
        self.progress.stopAnimation()
        self.ui.label_status.setText('Recipe has been saved')

        # unlock the form
        self.lock_the_form(False)

        # proceed with ok button
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_the_form(False)

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def lock_the_form(self, lock=True):
        """
        This method will lock the form so the user can use anything while its busy
        :return: void
        """
        self.ui.line_recipe_name.setEnabled(not lock)
        self.ui.table_recipe.setEnabled(not lock)
        self.ui.group_recipe.setEnabled(not lock)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)

########################################################
#         Supplimental classes for the dialog          #
########################################################


class ComboBoxDelegate(QItemDelegate):
    """
    A delegate that places a fully functioning QComboBox in every
    cell of the column to which it's applied
    """
    # first int is table row
    # second int is the item index
    item_selected = pyqtSignal(['int', 'int'], name='item_selected')

    def __init__(self, ingridients, items, parent):
        """

        :param ingridients:
        :param items:
        :param parent:
        :return:
        """
        super(ComboBoxDelegate, self).__init__(parent)
        self.items = items
        self.ingridients = ingridients

    def createEditor(self, parent, option, index):
        """

        :param parent:
        :param option:
        :param index:
        :return:
        """
        combo = QComboBox(parent)

        # make a list of items
        combo.addItems(map(lambda item: item.name, self.items))

        # disable items that have been used already
        for ing in self.ingridients:
            combo.model().item(ing['item_index']).setEnabled(False)

        return combo

    def setEditorData(self, editor, index):
        """

        :param editor:
        :param index:
        :return:
        """
        editor.blockSignals(True)

        if index.row() < len(self.ingridients):
            item_index = self.ingridients[index.row()]['item_index']
            editor.setCurrentIndex(item_index)
            editor.model().item(item_index).setEnabled(True)
        else:
            for i in range(editor.count()):
                if editor.model().item(i).isEnabled():
                    editor.setCurrentIndex(i)
                    break

        editor.blockSignals(False)

    def setModelData(self, editor, model, index):
        """

        :param editor:
        :param model:
        :param index:
        :return:
        """
        self.item_selected.emit(index.row(), editor.currentIndex())
        model.setData(index, self.items[editor.currentIndex()].name)


class SpinBoxDelegate(QItemDelegate):
    """
    A delegate that places a fully functioning QDoubleSpinBox in every
    cell of the column to which it's applied
    """
    # signals the amount has been entered
    amount_entered = pyqtSignal(['int', 'float'], name='amount_entered')

    def __init__(self, ingridients, items, parent):
        """

        :param ingridients:
        :param items:
        :param parent:
        :return:
        """
        super(SpinBoxDelegate, self).__init__(parent)
        self.items = items
        self.ingridients = ingridients

    def createEditor(self, parent, option, index):
        """
        This is the editor for the cell; i.e. this is the spinbox that is in the cell
        for the user to make a recipe by selecting ingredients in their inventory.
        :param parent: Parent class for the
        :param option:
        :param index:
        :return:
        """
        spin = QDoubleSpinBox(parent)
        spin.setDecimals(2)
        spin.setMinimum(0.01)
        spin.setMaximum(9999999999.99)
        return spin

    def setEditorData(self, editor, index):
        """
        this sets the data for the
        :param editor:
        :param index:
        :return:
        """
        editor.blockSignals(True)

        # find units
        ingridient = self.ingridients[index.row()]
        units = self.items[ingridient['item_index']].units
        editor.setSuffix(' ' + units)

        # find value
        editor.setValue(ingridient['amount'])

        editor.blockSignals(False)

    def setModelData(self, editor, model, index):
        """

        :param editor:
        :param model:
        :param index:
        :return: void
        """
        self.amount_entered.emit(index.row(), editor.value())
        units = self.items[self.ingridients[index.row()]['item_index']].units
        model.setData(index, str(editor.value()) + ' ' + units)
