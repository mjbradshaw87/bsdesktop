from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QMessageBox
import time
import datetime
from custom_controls.qprogressindicator import QProgressIndicator
from data import const
from forms.ui_kettle_dialog import Ui_KettleDialog

__author__ = 'michaelbradshaw'


class KettleDialog(QDialog):
    """
    This is the kettle dialog class.  It will allow the user to update
    any info for the kettle group box
    """
    # signal to report successful server interaction
    kettle_info_updated = pyqtSignal(['PyQt_PyObject'], name='kettle_info_updated')

    # noinspection PyUnresolvedReferences
    def __init__(self, data_engine, brew, parent=None):
        """

        :param data_engine:
        :param brew:
        :param parent:
        :return: void
        """
        super(KettleDialog, self).__init__(parent)

        # setup the ui
        self.ui = Ui_KettleDialog()
        self.ui.setupUi(self)

        # setup the objects
        self.brew = brew
        self.is_edit = len(self.brew['kettle_info']) > 0
        self.data_engine = data_engine
        self.progress = QProgressIndicator(self)
        self.ui.layout_status.insertWidget(0, self.progress)
        self.ui.label_status.setVisible(False)

        # setup behavior
        self.ui.label_status.setText(
            'Saving' if not self.is_edit else 'Updating' + ' the kettle info...'
        )
        self.setWindowTitle(
            'New' if not self.is_edit else 'Edit' + 'Kettle Info'
        )

        # set the maximumd date/times for the date objects
        self.ui.date_start_boil.setMaximumDateTime(datetime.datetime.now())
        self.ui.date_end_boil.setMaximumDateTime(datetime.datetime.now())

        # set up the dialog if there is info to fill
        if self.is_edit:
            # get the dates first
            boil_start_date = datetime.datetime.fromtimestamp(int(self.brew['kettle_info']['start_boil']))
            boil_end_date = datetime.datetime.fromtimestamp(int(self.brew['kettle_info']['end_boil']))

            # set all values
            self.ui.date_start_boil.setDateTime(boil_start_date)
            self.ui.date_end_boil.setDateTime(boil_end_date)
            self.ui.double_full_bbl.setValue(float(self.brew['kettle_info']['full_bbl']))
            self.ui.double_kettle_full.setValue(float(self.brew['kettle_info']['kettle_full']))
            self.ui.double_final_bbl.setValue(float(self.brew['kettle_info']['final_bbl']))
            self.ui.double_calc_up.setValue(float(self.brew['kettle_info']['calc_top_up']))
            self.ui.double_total_evap.setValue(float(self.brew['kettle_info']['total_evaporation']) * 100)
            self.ui.double_per_hour.setValue(float(self.brew['kettle_info']['per_hour']))
            self.ui.double_water_added.setValue(float(self.brew['kettle_info']['water_added']))
        else:
            # set todays date
            self.ui.date_start_boil.setDateTime(datetime.datetime.now())
            self.ui.date_end_boil.setDateTime(datetime.datetime.now())

        # setup signal
        self.ui.button_box.accepted.connect(self.ok_button_clicked)
        self.ui.button_box.rejected.connect(self.reject)

#########################################################
#                         slots                         #
#########################################################

    def ok_button_clicked(self):
        """
        This is the slot for the ok button on the button_box.  If clicked,
        this method will call the connect the data engine, gather the params
        and set up the call to the server.
        :return: void
        """
        # display actions and lock form
        self.lock_the_form()

        # data_engine connections
        self.data_engine.response_received.connect(self.kettle_edit_saved)
        self.data_engine.got_error.connect(self.server_error_received)

        # set up params
        # start time is being converted to UTC before sending
        start_boil_utc = time.mktime(self.ui.date_start_boil.dateTime().toPyDateTime().timetuple())
        end_boil_utc = time.mktime(self.ui.date_end_boil.dateTime().toPyDateTime().timetuple())
        total_evap = (float(self.ui.double_total_evap.value()) / 100)
        print(total_evap)

        # setup params
        params = {
            'start_boil': start_boil_utc,
            'end_boil': end_boil_utc,
            'full_bbl': float(self.ui.double_full_bbl.value()),
            'kettle_full': float(self.ui.double_kettle_full.value()),
            'final_bbl': float(self.ui.double_final_bbl.value()),
            'calc_top_up': float(self.ui.double_calc_up.value()),
            'total_evaporation': total_evap,
            'per_hour': float(self.ui.double_per_hour.value()),
            'water_added': float(self.ui.double_water_added.value()),
        }

        # if edit mode
        if self.is_edit:
            params['kettle_info_id'] = self.brew['kettle_info']['id']
        else:
            params['brew_id'] = self.brew['id']

        print(params)
        print(const.EDIT_KETTLE_INFO_URL if self.is_edit else const.ADD_KETTLE_INFO_URL)

        # # send to the server
        self.data_engine.send_request(
            url=const.EDIT_KETTLE_INFO_URL if self.is_edit else const.ADD_KETTLE_INFO_URL,
            params=params,
            request_type='POST'
        )

#########################################################
#             data engine slots                         #
#########################################################

    def kettle_edit_saved(self, response):
        """
        This is the slot for the server response.  If there is no error, then the server
        will call this slot and will close the form.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # unlock form
        self.lock_the_form(False)

        # report success
        self.kettle_info_updated.emit(response['kettle_info'])

        # exit dialog
        self.accept()

    def server_error_received(self, error):
        """
        This method is the slot for the server error signal.  If there is an error with the server,
        the signal will be broadcast by the data_engine and picked up here.  The error will be displayed
        in a message box so the user can report it.
        :param error: The error message form the server if there is a problem with a send request.
        it will display a code to help figure out the problem.
        :return: void
        """
        # close the connection
        try:
            self.data_engine.response_received.disconnect()
            self.data_engine.got_error.disconnect()
        except:
            pass

        # the form is no longer busy
        self.lock_the_form(False)

        # set up the message box
        box = QMessageBox(self)
        box.setText(str(error))
        box.exec_()

#########################################################
#                  internal  methods                    #
#########################################################

    def lock_the_form(self, lock=True):
        """
        This method will lock the form so the user can use anything while its busy.
        :return: void
        """
        self.ui.group_kettle.setEnabled(not lock)
        self.ui.button_box.button(QDialogButtonBox.Ok).setEnabled(not lock)
        self.ui.label_status.setHidden(not lock)
        self.progress.startAnimation() if lock else self.progress.stopAnimation()
